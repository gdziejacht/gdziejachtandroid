package com.gdziejacht.gui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import com.gdziejacht.PseudoContainer;
import com.gdziejacht.DataModels.LocalPhoto;
import com.gdziejacht.contracts.IDatabaseHelper;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.File;
import java.io.IOException;


public class CameraActivity extends SuperActivity {
    static final int REQUEST_TAKE_PHOTO = 100;
    static final String PHOTOS_FOLDER_NAME="gdziejacht";
    public static final String DATE_TIME_FORMATTER_STRING="yyyyMMdd_HHmmss";
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.forPattern(DATE_TIME_FORMATTER_STRING);
    File currentFile; //na potrzeby czyszczenia w przypadku nie powodzenia
    String shootTime;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO) {
            if (resultCode == RESULT_OK) {
                //Bundle extras = data.getExtras();
                //Bitmap imageBitmap = (Bitmap) extras.get("data");
                //TODO: moze skoro galeria i tak musi wiedziec o localphoto to niech tam sie tworzy obiekt? to tu importa mozna by capnac
                LocalPhoto localPhoto = new LocalPhoto(currentFile.getAbsolutePath(), state.getActualCheckpoint());
                IDatabaseHelper helper= PseudoContainer.Instance.getSingleton(IDatabaseHelper.class);
                helper.createPhoto(localPhoto);
                //GalleryActivity.addPhotoToArray(localPhoto);
                
                connector.sendPhoto(localPhoto);

            } else if (resultCode == RESULT_CANCELED) {
                if(currentFile!=null)
                    currentFile.delete();
            } else {
                if(currentFile!=null)
                    currentFile.delete();
            }
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    public static File getDir() {
        File sdDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File gdzieJachtDir=new File(sdDir, PHOTOS_FOLDER_NAME);
        if(!gdzieJachtDir.exists())
            gdzieJachtDir.mkdir();
        return gdzieJachtDir;
    }

    private File createImageFile() throws IOException {
        String timeStamp = DateTime.now().toString(DATE_TIME_FORMATTER_STRING);
        shootTime=timeStamp;
        String imageFileName = timeStamp; //TODO: dac roznienie miedzy userami gdy bedziemy korzystac z logowania. osobne foldery chyba maja najwiecej sensu
        File storageDir = getDir();
        //glupi temp file zasmieca filename
        currentFile = new File(storageDir,imageFileName+".jpg");
        //mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return currentFile;
    }
}
