package com.gdziejacht.commons;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import com.gdziejacht.contracts.IProperty;


public abstract class AbstractProperty implements IProperty {

    protected PropertyChangeSupport listeners = new PropertyChangeSupport(this);

    //Adding and removing property change listeners
    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        listeners.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        listeners.removePropertyChangeListener(listener);

    }

    @Override
    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        listeners.addPropertyChangeListener(propertyName, listener);

    }

    @Override
    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        listeners.removePropertyChangeListener(propertyName, listener);

    }
}
