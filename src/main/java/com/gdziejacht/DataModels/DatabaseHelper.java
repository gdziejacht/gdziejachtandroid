package com.gdziejacht.DataModels;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.gdziejacht.R;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

/**
 * Database helper class used to manage the creation and upgrading of your database. This class also usually provides
 * the DAOs used by the other classes.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

	// name of the database file for your application -- change to something appropriate for your app
	private static final String DATABASE_NAME = "gdziejacht.db";
	// any time you make changes to your database objects, you may have to increase the database version
	private static final int DATABASE_VERSION = 1;

	// the DAO object we use to access the SimpleData table
	private Dao<LocalCheckpoint, Integer> checkpointDao = null;
	//private RuntimeExceptionDao<LocalCheckpoint, Integer> simpleRuntimeDao = null;
	private Dao<LocalTrip, Integer> localTripDao;
	private Dao<LocalPhoto, Integer> localPhotoDao;
	private Dao<LocalUser, Integer> localUserDao;

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
	}

	/**
	 * This is called when the database is first created. Usually you should call createTable statements here to create
	 * the tables that will store your data.
	 */
	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
		try {
			Log.i(DatabaseHelper.class.getName(), "onCreate");
			TableUtils.createTable(connectionSource, LocalCheckpoint.class);
			TableUtils.createTable(connectionSource, LocalTrip.class);
			TableUtils.createTable(connectionSource, LocalPhoto.class);
			TableUtils.createTable(connectionSource, LocalUser.class);
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
			throw new RuntimeException(e);
		}

		// here we try inserting data in the on-create as a test
		//RuntimeExceptionDao<LocalCheckpoint, Integer> dao = getLocalCheckpointDataDao();
		//long millis = System.currentTimeMillis();
		// create some entries in the onCreate
		//LocalCheckpoint simple = new LocalCheckpoint();
		//dao.create(simple);
		//simple = new LocalCheckpoint();
		//dao.create(simple);
		//Log.i(DatabaseHelper.class.getName(), "created new entries in onCreate: " + millis);
	}

	/**
	 * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
	 * the various data to match the new version number.
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
		try {
			Log.i(DatabaseHelper.class.getName(), "onUpgrade");
			TableUtils.dropTable(connectionSource, LocalCheckpoint.class, true);
			TableUtils.dropTable(connectionSource, LocalTrip.class, true);
			TableUtils.dropTable(connectionSource, LocalPhoto.class, true);
			TableUtils.dropTable(connectionSource, LocalUser.class, true);
			// after we drop the old databases, we create the new ones
			onCreate(db, connectionSource);
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "Can't drop databases", e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Returns the Database Access Object (DAO) for our SimpleData class. It will create it or just give the cached
	 * value.
	 */
	public synchronized Dao<LocalCheckpoint, Integer> getLocalCheckpointDao() throws SQLException {
		if (checkpointDao == null) {
			
			checkpointDao = getDao(LocalCheckpoint.class);
		}
		return checkpointDao;
	}
	public synchronized Dao<LocalTrip, Integer> getLocalTripDao() throws SQLException {
		if (localTripDao == null) {
			localTripDao = getDao(LocalTrip.class);
		}
		return localTripDao;
	}
	public synchronized Dao<LocalPhoto, Integer> getLocalPhotoDao() throws SQLException {
		if (localPhotoDao == null) {
			localPhotoDao = getDao(LocalPhoto.class);
		}
		return localPhotoDao;
	}
	public synchronized Dao<LocalUser, Integer> getLocalUserDao() throws SQLException {
		if (localUserDao == null) {
			localUserDao = getDao(LocalUser.class);
		}
		return localUserDao;
	}
	/**
	 * Returns the RuntimeExceptionDao (Database Access Object) version of a Dao for our SimpleData class. It will
	 * create it or just give the cached value. RuntimeExceptionDao only through RuntimeExceptions.
	 */
//	public RuntimeExceptionDao<LocalCheckpoint, Integer> getLocalCheckpointDataDao() {
//		if (simpleRuntimeDao == null) {
//			simpleRuntimeDao = getRuntimeExceptionDao(LocalCheckpoint.class);
//		}
//		return simpleRuntimeDao;
//	}

	/**
	 * Close the database connections and clear any cached DAOs.
	 */
	@Override
	public void close() {
		super.close();
		checkpointDao = null;
		localPhotoDao=null;
		localTripDao=null;
		localUserDao=null;
		//simpleRuntimeDao = null;
	}
}