package com.gdziejacht.contracts;

import java.util.List;

import com.gdziejacht.DataModels.LocalCheckpoint;
import com.gdziejacht.DataModels.LocalTrip;
import com.gdziejacht.DataModels.LocalUser;
import com.gdziejacht.contracts.IoC.ISerializable;

public interface IState extends IProperty, ISerializable{
	static final String ACTION_LOGIN = "ActionLogin";
	static final String ACTION_BEGINTRIP = "ActionBeginTrip";
	static final String ACTION_POSITION_CHANGED = "ActionPositionChanged";
	static final String ACTION_GPSSTATE_CHANGED = "ActionGpsStateChanged";
	void setLoggedUser(LocalUser localuser);
	LocalUser getLoggedUser();
	LocalTrip getActualTrip();
	void setActualCheckpoint(LocalCheckpoint checkpoint);
	LocalCheckpoint getActualCheckpoint();
	void finishTrackingTrip();
	LocalTrip createTrackingTrip();
	List<LocalTrip> getAllTrips();
	void setGpsEnabled(boolean gpsState);
	void setVisibleTrip(LocalTrip trip);
	LocalTrip getVisibleTrip();
	void reSendVisibleTrip();
	boolean isGpsEnabled();
	void deleteTrip(LocalTrip trip);
	void setLoggedUser(int userId);

}