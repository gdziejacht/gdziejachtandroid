package com.gdziejacht.service.internal;

import com.gdziejacht.errorHandling.ConnectionException;
import com.gdziejacht.errorHandling.WebServiceException;
import com.gdziejacht.service.internal.WS_Enums.*;

import java.util.List;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.ksoap2.HeaderProperty;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

import android.os.AsyncTask;

import org.ksoap2.serialization.MarshalFloat;

public class GdzieJachtService {

	public String NAMESPACE = "http://gdzie-jacht.pl/webservice";
	public String url = "http://gdzie-jacht.pl/webservice/GdzieJachtService.svc";
	public int timeOut = 60000;
	public SoapProtocolVersion soapVersion;

	public GdzieJachtService() {
		//experimental as fuck
		System.setProperty("http.maxConnections", "5");
	}

//	public GdzieJachtService(IWsdl2CodeEvents eventHandler) {
//		//this.eventHandler = eventHandler;
//	}
//
//	public GdzieJachtService(IWsdl2CodeEvents eventHandler, String url) {
//		//this.eventHandler = eventHandler;
//		this.url = url;
//	}
//
//	public GdzieJachtService(IWsdl2CodeEvents eventHandler, String url,
//			int timeOutInSeconds) {
//		//this.eventHandler = eventHandler;
//		this.url = url;
//		this.setTimeOut(timeOutInSeconds);
//	}

	public void setTimeOut(int seconds) {
		this.timeOut = seconds * 1000;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void RegisterAsync(RegisterContract registrationData,IWsdl2CodeEvents<User, RegisterContract> eventHandler){
		RegisterAsync(registrationData, eventHandler, null);
	}

	public void RegisterAsync(final RegisterContract registrationData, final IWsdl2CodeEvents<User, RegisterContract> eventHandler,
			final List<HeaderProperty> headers) {

		new AsyncTask<Void, Void, User>() {
			@Override
			protected void onPreExecute() {
				eventHandler.Wsdl2CodeStartedRequest();
			};

			@Override
			protected User doInBackground(Void... params) {
				return Register(registrationData, eventHandler, headers);
			}

			@Override
			protected void onPostExecute(User result) {
				eventHandler.Wsdl2CodeEndedRequest();
				if (result != null) {
					eventHandler.Wsdl2CodeFinished(result);
				}
			}
		}.execute();
	}

//	private User Register(RegisterContract registrationData) {
//		return Register(registrationData, null);
//	}

	private User Register(RegisterContract registrationData,
			IWsdl2CodeEvents<User, RegisterContract> eventHandler, List<HeaderProperty> headers) {
		SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		soapEnvelope.implicitTypes = true;
		soapEnvelope.dotNet = true;
		SoapObject soapReq = new SoapObject("http://gdzie-jacht.pl/webservice",
				"Register");
		soapEnvelope.addMapping("http://gdzie-jacht.pl/webservice",
				"registrationData", new RegisterContract().getClass());
		soapReq.addProperty("registrationData", registrationData);
		soapEnvelope.setOutputSoapObject(soapReq);
		HttpTransportSE httpTransport = new HttpTransportSE(url, timeOut);
		try {
			if (headers != null) {
				httpTransport
						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/Register",
								soapEnvelope, headers);
			} else {
				httpTransport
						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/Register",
								soapEnvelope);
			}
			Object retObj = soapEnvelope.bodyIn;
			if (retObj instanceof SoapFault) {
				SoapFault fault = (SoapFault) retObj;
				Exception ex = new WebServiceException(fault);
				if (eventHandler != null)
					eventHandler.Wsdl2CodeFinishedWithException(ex, registrationData);
			} else {
				SoapObject result = (SoapObject) retObj;
				if (result.getPropertyCount() > 0) {
					Object obj = result.getProperty(0);
					SoapObject j = (SoapObject) obj;
					User resultVariable = new User(j);
					return resultVariable;

				}
			}
		} catch (Exception e) {
			if (eventHandler != null)
				eventHandler.Wsdl2CodeFinishedWithException(new ConnectionException(e), registrationData);
		}
		return null;
	}

	public void GetUserAsync(String user_name, String user_password,
			AuthenticationInfo authenticationInfo, IWsdl2CodeEvents<User, Object> eventHandler) {
		GetUserAsync(user_name, user_password, authenticationInfo, eventHandler,  null);
	}

	public void GetUserAsync(final String user_name,
			final String user_password,
			final AuthenticationInfo authenticationInfo,
			final IWsdl2CodeEvents<User, Object> eventHandler, final List<HeaderProperty> headers) {

		new AsyncTask<Void, Void, User>() {
			@Override
			protected void onPreExecute() {
				eventHandler.Wsdl2CodeStartedRequest();
			};

			@Override
			protected User doInBackground(Void... params) {
				return GetUser(user_name, user_password, authenticationInfo,eventHandler, 
						headers);
			}

			@Override
			protected void onPostExecute(User result) {
				eventHandler.Wsdl2CodeEndedRequest();
				if (result != null) {
					eventHandler.Wsdl2CodeFinished(result);
				}
			}
		}.execute();
	}

//	public User GetUser(String user_name, String user_password,
//			AuthenticationInfo authenticationInfo) {
//		return GetUser(user_name, user_password, authenticationInfo, null);
//	}

	private User GetUser(String user_name, String user_password,
			AuthenticationInfo authenticationInfo, IWsdl2CodeEvents<User, Object> eventHandler, List<HeaderProperty> headers) {
		SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		soapEnvelope.implicitTypes = true;
		soapEnvelope.dotNet = true;
		SoapObject soapReq = new SoapObject("http://gdzie-jacht.pl/webservice",
				"GetUser");
		soapEnvelope.addMapping("http://gdzie-jacht.pl/webservice",
				"authenticationInfo", new AuthenticationInfo().getClass());
		soapReq.addProperty("user_name", user_name);
		soapReq.addProperty("user_password", user_password);
		soapReq.addProperty("authenticationInfo", authenticationInfo);
		soapEnvelope.setOutputSoapObject(soapReq);
		HttpTransportSE httpTransport = new HttpTransportSE(url, timeOut);
		try {
			if (headers != null) {
				httpTransport
						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/GetUser",
								soapEnvelope, headers);
			} else {
				httpTransport
						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/GetUser",
								soapEnvelope);
			}
			Object retObj = soapEnvelope.bodyIn;
			if (retObj instanceof SoapFault) {
				SoapFault fault = (SoapFault) retObj;
				Exception ex = new WebServiceException(fault);
				if (eventHandler != null)
					eventHandler.Wsdl2CodeFinishedWithException(ex, null);
			} else {
				SoapObject result = (SoapObject) retObj;
				if (result.getPropertyCount() > 0) {
					Object obj = result.getProperty(0);
					SoapObject j = (SoapObject) obj;
					User resultVariable = new User(j);
					return resultVariable;

				}
			}
		} catch (Exception e) {
			if (eventHandler != null)
				eventHandler.Wsdl2CodeFinishedWithException(new ConnectionException(e), null);
		}
		return null;
	}

//	public void GetUsersListAsync(AuthenticationInfo authenticationInfo)
//			throws Exception {
//		if (this.eventHandler == null)
//			throw new Exception("Async Methods Requires IWsdl2CodeEvents");
//		GetUsersListAsync(authenticationInfo, null);
//	}
//
//	public void GetUsersListAsync(final AuthenticationInfo authenticationInfo,
//			final List<HeaderProperty> headers) throws Exception {
//
//		new AsyncTask<Void, Void, VectorUser>() {
//			@Override
//			protected void onPreExecute() {
//				eventHandler.Wsdl2CodeStartedRequest();
//			};
//
//			@Override
//			protected VectorUser doInBackground(Void... params) {
//				return GetUsersList(authenticationInfo, headers);
//			}
//
//			@Override
//			protected void onPostExecute(VectorUser result) {
//				eventHandler.Wsdl2CodeEndedRequest();
//				if (result != null) {
//					eventHandler.Wsdl2CodeFinished("GetUsersList", result);
//				}
//			}
//		}.execute();
//	}
//
//	public VectorUser GetUsersList(AuthenticationInfo authenticationInfo) {
//		return GetUsersList(authenticationInfo, null);
//	}
//
//	public VectorUser GetUsersList(AuthenticationInfo authenticationInfo,
//			List<HeaderProperty> headers) {
//		SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(
//				SoapEnvelope.VER11);
//		soapEnvelope.implicitTypes = true;
//		soapEnvelope.dotNet = true;
//		SoapObject soapReq = new SoapObject("http://gdzie-jacht.pl/webservice",
//				"GetUsersList");
//		soapEnvelope.addMapping("http://gdzie-jacht.pl/webservice",
//				"authenticationInfo", new AuthenticationInfo().getClass());
//		soapReq.addProperty("authenticationInfo", authenticationInfo);
//		soapEnvelope.setOutputSoapObject(soapReq);
//		HttpTransportSE httpTransport = new HttpTransportSE(url, timeOut);
//		try {
//			if (headers != null) {
//				httpTransport
//						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/GetUsersList",
//								soapEnvelope, headers);
//			} else {
//				httpTransport
//						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/GetUsersList",
//								soapEnvelope);
//			}
//			Object retObj = soapEnvelope.bodyIn;
//			if (retObj instanceof SoapFault) {
//				SoapFault fault = (SoapFault) retObj;
//				Exception ex = new Exception(fault.faultstring);
//				if (eventHandler != null)
//					eventHandler.Wsdl2CodeFinishedWithException(ex);
//			} else {
//				SoapObject result = (SoapObject) retObj;
//				if (result.getPropertyCount() > 0) {
//					Object obj = result.getProperty(0);
//					SoapObject j = (SoapObject) obj;
//					VectorUser resultVariable = new VectorUser(j);
//					return resultVariable;
//				}
//			}
//		} catch (Exception e) {
//			if (eventHandler != null)
//				eventHandler.Wsdl2CodeFinishedWithException(e);
//			e.printStackTrace();
//		}
//		return null;
//	}
//
//	public void GetFriendsAsync(int userId, boolean userIdSpecified,
//			AuthenticationInfo authenticationInfo) throws Exception {
//		if (this.eventHandler == null)
//			throw new Exception("Async Methods Requires IWsdl2CodeEvents");
//		GetFriendsAsync(userId, userIdSpecified, authenticationInfo, null);
//	}
//
//	public void GetFriendsAsync(final int userId,
//			final boolean userIdSpecified,
//			final AuthenticationInfo authenticationInfo,
//			final List<HeaderProperty> headers) throws Exception {
//
//		new AsyncTask<Void, Void, VectorUser>() {
//			@Override
//			protected void onPreExecute() {
//				eventHandler.Wsdl2CodeStartedRequest();
//			};
//
//			@Override
//			protected VectorUser doInBackground(Void... params) {
//				return GetFriends(userId, userIdSpecified, authenticationInfo,
//						headers);
//			}
//
//			@Override
//			protected void onPostExecute(VectorUser result) {
//				eventHandler.Wsdl2CodeEndedRequest();
//				if (result != null) {
//					eventHandler.Wsdl2CodeFinished("GetFriends", result);
//				}
//			}
//		}.execute();
//	}
//
//	public VectorUser GetFriends(int userId, boolean userIdSpecified,
//			AuthenticationInfo authenticationInfo) {
//		return GetFriends(userId, userIdSpecified, authenticationInfo, null);
//	}
//
//	public VectorUser GetFriends(int userId, boolean userIdSpecified,
//			AuthenticationInfo authenticationInfo, List<HeaderProperty> headers) {
//		SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(
//				SoapEnvelope.VER11);
//		soapEnvelope.implicitTypes = true;
//		soapEnvelope.dotNet = true;
//		SoapObject soapReq = new SoapObject("http://gdzie-jacht.pl/webservice",
//				"GetFriends");
//		soapEnvelope.addMapping("http://gdzie-jacht.pl/webservice",
//				"authenticationInfo", new AuthenticationInfo().getClass());
//		soapReq.addProperty("userId", userId);
//		soapReq.addProperty("userIdSpecified", userIdSpecified);
//		soapReq.addProperty("authenticationInfo", authenticationInfo);
//		soapEnvelope.setOutputSoapObject(soapReq);
//		HttpTransportSE httpTransport = new HttpTransportSE(url, timeOut);
//		try {
//			if (headers != null) {
//				httpTransport
//						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/GetFriends",
//								soapEnvelope, headers);
//			} else {
//				httpTransport
//						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/GetFriends",
//								soapEnvelope);
//			}
//			Object retObj = soapEnvelope.bodyIn;
//			if (retObj instanceof SoapFault) {
//				SoapFault fault = (SoapFault) retObj;
//				Exception ex = new Exception(fault.faultstring);
//				if (eventHandler != null)
//					eventHandler.Wsdl2CodeFinishedWithException(ex);
//			} else {
//				SoapObject result = (SoapObject) retObj;
//				if (result.getPropertyCount() > 0) {
//					Object obj = result.getProperty(0);
//					SoapObject j = (SoapObject) obj;
//					VectorUser resultVariable = new VectorUser(j);
//					return resultVariable;
//				}
//			}
//		} catch (Exception e) {
//			if (eventHandler != null)
//				eventHandler.Wsdl2CodeFinishedWithException(e);
//			e.printStackTrace();
//		}
//		return null;
//	}
//
//	public void AddFriendAsync(int userId, boolean userIdSpecified,
//			int friendId, boolean friendIdSpecified,
//			AuthenticationInfo authenticationInfo) throws Exception {
//		if (this.eventHandler == null)
//			throw new Exception("Async Methods Requires IWsdl2CodeEvents");
//		AddFriendAsync(userId, userIdSpecified, friendId, friendIdSpecified,
//				authenticationInfo, null);
//	}
//
//	public void AddFriendAsync(final int userId, final boolean userIdSpecified,
//			final int friendId, final boolean friendIdSpecified,
//			final AuthenticationInfo authenticationInfo,
//			final List<HeaderProperty> headers) throws Exception {
//
//		new AsyncTask<Void, Void, Void>() {
//			@Override
//			protected void onPreExecute() {
//				eventHandler.Wsdl2CodeStartedRequest();
//			};
//
//			@Override
//			protected Void doInBackground(Void... params) {
//				AddFriend(userId, userIdSpecified, friendId, friendIdSpecified,
//						authenticationInfo, headers);
//				return null;
//			}
//
//			@Override
//			protected void onPostExecute(Void result) {
//				eventHandler.Wsdl2CodeEndedRequest();
//				if (result != null) {
//					eventHandler.Wsdl2CodeFinished("AddFriend", result);
//				}
//			}
//		}.execute();
//	}
//
//	public void AddFriend(int userId, boolean userIdSpecified, int friendId,
//			boolean friendIdSpecified, AuthenticationInfo authenticationInfo) {
//		AddFriend(userId, userIdSpecified, friendId, friendIdSpecified,
//				authenticationInfo, null);
//	}
//
//	public void AddFriend(int userId, boolean userIdSpecified, int friendId,
//			boolean friendIdSpecified, AuthenticationInfo authenticationInfo,
//			List<HeaderProperty> headers) {
//		SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(
//				SoapEnvelope.VER11);
//		soapEnvelope.implicitTypes = true;
//		soapEnvelope.dotNet = true;
//		SoapObject soapReq = new SoapObject("http://gdzie-jacht.pl/webservice",
//				"AddFriend");
//		soapEnvelope.addMapping("http://gdzie-jacht.pl/webservice",
//				"authenticationInfo", new AuthenticationInfo().getClass());
//		soapReq.addProperty("userId", userId);
//		soapReq.addProperty("userIdSpecified", userIdSpecified);
//		soapReq.addProperty("friendId", friendId);
//		soapReq.addProperty("friendIdSpecified", friendIdSpecified);
//		soapReq.addProperty("authenticationInfo", authenticationInfo);
//		soapEnvelope.setOutputSoapObject(soapReq);
//		HttpTransportSE httpTransport = new HttpTransportSE(url, timeOut);
//		try {
//			if (headers != null) {
//				httpTransport
//						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/AddFriend",
//								soapEnvelope, headers);
//			} else {
//				httpTransport
//						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/AddFriend",
//								soapEnvelope);
//			}
//		} catch (Exception e) {
//			if (eventHandler != null)
//				eventHandler.Wsdl2CodeFinishedWithException(e);
//			e.printStackTrace();
//		}
//	}
//
//	public void RemoveFriendAsync(int userId, boolean userIdSpecified,
//			int friendId, boolean friendIdSpecified,
//			AuthenticationInfo authenticationInfo) throws Exception {
//		if (this.eventHandler == null)
//			throw new Exception("Async Methods Requires IWsdl2CodeEvents");
//		RemoveFriendAsync(userId, userIdSpecified, friendId, friendIdSpecified,
//				authenticationInfo, null);
//	}
//
//	public void RemoveFriendAsync(final int userId,
//			final boolean userIdSpecified, final int friendId,
//			final boolean friendIdSpecified,
//			final AuthenticationInfo authenticationInfo,
//			final List<HeaderProperty> headers) throws Exception {
//
//		new AsyncTask<Void, Void, Void>() {
//			@Override
//			protected void onPreExecute() {
//				eventHandler.Wsdl2CodeStartedRequest();
//			};
//
//			@Override
//			protected Void doInBackground(Void... params) {
//				RemoveFriend(userId, userIdSpecified, friendId,
//						friendIdSpecified, authenticationInfo, headers);
//				return null;
//			}
//
//			@Override
//			protected void onPostExecute(Void result) {
//				eventHandler.Wsdl2CodeEndedRequest();
//				if (result != null) {
//					eventHandler.Wsdl2CodeFinished("RemoveFriend", result);
//				}
//			}
//		}.execute();
//	}
//
//	public void RemoveFriend(int userId, boolean userIdSpecified, int friendId,
//			boolean friendIdSpecified, AuthenticationInfo authenticationInfo) {
//		RemoveFriend(userId, userIdSpecified, friendId, friendIdSpecified,
//				authenticationInfo, null);
//	}
//
//	public void RemoveFriend(int userId, boolean userIdSpecified, int friendId,
//			boolean friendIdSpecified, AuthenticationInfo authenticationInfo,
//			List<HeaderProperty> headers) {
//		SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(
//				SoapEnvelope.VER11);
//		soapEnvelope.implicitTypes = true;
//		soapEnvelope.dotNet = true;
//		SoapObject soapReq = new SoapObject("http://gdzie-jacht.pl/webservice",
//				"RemoveFriend");
//		soapEnvelope.addMapping("http://gdzie-jacht.pl/webservice",
//				"authenticationInfo", new AuthenticationInfo().getClass());
//		soapReq.addProperty("userId", userId);
//		soapReq.addProperty("userIdSpecified", userIdSpecified);
//		soapReq.addProperty("friendId", friendId);
//		soapReq.addProperty("friendIdSpecified", friendIdSpecified);
//		soapReq.addProperty("authenticationInfo", authenticationInfo);
//		soapEnvelope.setOutputSoapObject(soapReq);
//		HttpTransportSE httpTransport = new HttpTransportSE(url, timeOut);
//		try {
//			if (headers != null) {
//				httpTransport
//						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/RemoveFriend",
//								soapEnvelope, headers);
//			} else {
//				httpTransport
//						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/RemoveFriend",
//								soapEnvelope);
//			}
//		} catch (Exception e) {
//			if (eventHandler != null)
//				eventHandler.Wsdl2CodeFinishedWithException(e);
//			e.printStackTrace();
//		}
//	}

	public void BeginTripAsync(Trip trip, AuthenticationInfo authenticationInfo, IWsdl2CodeEvents<Integer, Trip> eventHandler)
			{
		BeginTripAsync(trip, authenticationInfo, eventHandler,  null);
	}

	public void BeginTripAsync(final Trip trip,
			final AuthenticationInfo authenticationInfo,
			final IWsdl2CodeEvents<Integer, Trip> eventHandler, final List<HeaderProperty> headers) {

		new AsyncTask<Void, Void, Integer>() {
			@Override
			protected void onPreExecute() {
				eventHandler.Wsdl2CodeStartedRequest();
			};

			@Override
			protected Integer doInBackground(Void... params) {
				return BeginTrip(trip, authenticationInfo, eventHandler,  headers);
			}

			@Override
			protected void onPostExecute(Integer result) {
				eventHandler.Wsdl2CodeEndedRequest();
				if (result != null) {
					eventHandler.Wsdl2CodeFinished(result);
				}
			}
		}.execute();
	}

//	public Integer BeginTrip(Trip trip, AuthenticationInfo authenticationInfo) {
//		return BeginTrip(trip, authenticationInfo, null);
//	}

	private Integer BeginTrip(Trip trip, AuthenticationInfo authenticationInfo,
			IWsdl2CodeEvents<Integer, Trip> eventHandler, List<HeaderProperty> headers) {
		SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		soapEnvelope.implicitTypes = true;
		soapEnvelope.dotNet = true;
		SoapObject soapReq = new SoapObject("http://gdzie-jacht.pl/webservice",
				"BeginTrip");
		soapEnvelope.addMapping("http://gdzie-jacht.pl/webservice", "trip",
				new Trip().getClass());
		soapEnvelope.addMapping("http://gdzie-jacht.pl/webservice",
				"authenticationInfo", new AuthenticationInfo().getClass());
		soapReq.addProperty("trip", trip);
		soapReq.addProperty("authenticationInfo", authenticationInfo);
		soapEnvelope.setOutputSoapObject(soapReq);
		HttpTransportSE httpTransport = new HttpTransportSE(url, timeOut);
		httpTransport.debug = true;
		try {
			if (headers != null) {
				httpTransport
						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/BeginTrip",
								soapEnvelope, headers);
			} else {
				httpTransport
						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/BeginTrip",
								soapEnvelope);
			}
			Object retObj = soapEnvelope.bodyIn;
			if (retObj instanceof SoapFault) {
				SoapFault fault = (SoapFault) retObj;
				Exception ex = new WebServiceException(fault);
				if (eventHandler != null)
					eventHandler.Wsdl2CodeFinishedWithException(ex, trip);
			} else {
				SoapObject result = (SoapObject) retObj;
				if (result.getPropertyCount() > 0) {
					Object obj = result.getProperty(0);
					SoapPrimitive j = (SoapPrimitive) obj;
					String value = (String) j.getValue();
					return Integer.parseInt(value);
				}
			}
		} catch (Exception e) {
			if (eventHandler != null)
				eventHandler.Wsdl2CodeFinishedWithException(new ConnectionException(e), trip);
		}
		return null;
	}

//	public void GetTripAsync(int tripId, boolean tripIdSpecified,
//			AuthenticationInfo authenticationInfo) throws Exception {
//		if (this.eventHandler == null)
//			throw new Exception("Async Methods Requires IWsdl2CodeEvents");
//		GetTripAsync(tripId, tripIdSpecified, authenticationInfo, null);
//	}
//
//	public void GetTripAsync(final int tripId, final boolean tripIdSpecified,
//			final AuthenticationInfo authenticationInfo,
//			final List<HeaderProperty> headers) throws Exception {
//
//		new AsyncTask<Void, Void, Trip>() {
//			@Override
//			protected void onPreExecute() {
//				eventHandler.Wsdl2CodeStartedRequest();
//			};
//
//			@Override
//			protected Trip doInBackground(Void... params) {
//				return GetTrip(tripId, tripIdSpecified, authenticationInfo,
//						headers);
//			}
//
//			@Override
//			protected void onPostExecute(Trip result) {
//				eventHandler.Wsdl2CodeEndedRequest();
//				if (result != null) {
//					eventHandler.Wsdl2CodeFinished("GetTrip", result);
//				}
//			}
//		}.execute();
//	}
//
//	public Trip GetTrip(int tripId, boolean tripIdSpecified,
//			AuthenticationInfo authenticationInfo) {
//		return GetTrip(tripId, tripIdSpecified, authenticationInfo, null);
//	}
//
//	public Trip GetTrip(int tripId, boolean tripIdSpecified,
//			AuthenticationInfo authenticationInfo, List<HeaderProperty> headers) {
//		SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(
//				SoapEnvelope.VER11);
//		soapEnvelope.implicitTypes = true;
//		soapEnvelope.dotNet = true;
//		SoapObject soapReq = new SoapObject("http://gdzie-jacht.pl/webservice",
//				"GetTrip");
//		soapEnvelope.addMapping("http://gdzie-jacht.pl/webservice",
//				"authenticationInfo", new AuthenticationInfo().getClass());
//		soapReq.addProperty("tripId", tripId);
//		soapReq.addProperty("tripIdSpecified", tripIdSpecified);
//		soapReq.addProperty("authenticationInfo", authenticationInfo);
//		soapEnvelope.setOutputSoapObject(soapReq);
//		HttpTransportSE httpTransport = new HttpTransportSE(url, timeOut);
//		try {
//			if (headers != null) {
//				httpTransport
//						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/GetTrip",
//								soapEnvelope, headers);
//			} else {
//				httpTransport
//						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/GetTrip",
//								soapEnvelope);
//			}
//			Object retObj = soapEnvelope.bodyIn;
//			if (retObj instanceof SoapFault) {
//				SoapFault fault = (SoapFault) retObj;
//				Exception ex = new Exception(fault.faultstring);
//				if (eventHandler != null)
//					eventHandler.Wsdl2CodeFinishedWithException(ex);
//			} else {
//				SoapObject result = (SoapObject) retObj;
//				if (result.getPropertyCount() > 0) {
//					Object obj = result.getProperty(0);
//					SoapObject j = (SoapObject) obj;
//					Trip resultVariable = new Trip(j);
//					return resultVariable;
//
//				}
//			}
//		} catch (Exception e) {
//			if (eventHandler != null)
//				eventHandler.Wsdl2CodeFinishedWithException(e);
//			e.printStackTrace();
//		}
//		return null;
//	}

	public void SendTripCheckpointAsync(int userId,
			boolean userIdSpecified, Checkpoint checkpointData,
			AuthenticationInfo authenticationInfo,IWsdl2CodeEvents<Integer, Checkpoint> eventHandler)
	{
		SendTripCheckpointAsync(userId, userIdSpecified, checkpointData,
				authenticationInfo, eventHandler, null);
	}

	public void SendTripCheckpointAsync(final int userId,
			final boolean userIdSpecified, final Checkpoint checkpointData,
			final AuthenticationInfo authenticationInfo, final IWsdl2CodeEvents<Integer, Checkpoint> eventHandler,
			final List<HeaderProperty> headers) {

		new AsyncTask<Void, Void, Integer>() {
			@Override
			protected void onPreExecute() {
				eventHandler.Wsdl2CodeStartedRequest();
			};

			@Override
			protected Integer doInBackground(Void... params) {
				return SendTripCheckpoint(userId, userIdSpecified, checkpointData,
						authenticationInfo, eventHandler, headers);
				
			}

			@Override
			protected void onPostExecute(Integer result) {
				eventHandler.Wsdl2CodeEndedRequest();
				if (result != null) {
					eventHandler.Wsdl2CodeFinished(result);
				}
			}
		}.execute();
	}

//	public void SendTripCheckpoint(int userId, boolean userIdSpecified,
//			Checkpoint checkpointData, AuthenticationInfo authenticationInfo) {
//		SendTripCheckpoint(userId, userIdSpecified, checkpointData,
//				authenticationInfo, null);
//	}

	public Integer SendTripCheckpoint(int userId, boolean userIdSpecified,
			Checkpoint checkpointData, AuthenticationInfo authenticationInfo,
			IWsdl2CodeEvents<Integer,Checkpoint> eventHandler, List<HeaderProperty> headers) {
		SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		soapEnvelope.implicitTypes = true;
		soapEnvelope.dotNet = true;
		SoapObject soapReq = new SoapObject("http://gdzie-jacht.pl/webservice",
				"SendTripCheckpoint");
		soapEnvelope.addMapping("http://gdzie-jacht.pl/webservice",
				"checkpointData", new Checkpoint().getClass());
		MarshalFloat marshalFloat = new MarshalFloat();
		marshalFloat.register(soapEnvelope);
		soapEnvelope.addMapping("http://gdzie-jacht.pl/webservice",
				"authenticationInfo", new AuthenticationInfo().getClass());
		soapReq.addProperty("userId", userId);
		soapReq.addProperty("userIdSpecified", userIdSpecified);
		soapReq.addProperty("checkpointData", checkpointData);
		soapReq.addProperty("authenticationInfo", authenticationInfo);
		soapEnvelope.setOutputSoapObject(soapReq);
		HttpTransportSE httpTransport = new HttpTransportSE(url, timeOut);
		httpTransport.debug=true;
		try {
			if (headers != null) {
				httpTransport
						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/SendTripCheckpoint",
								soapEnvelope, headers);
			} else {
				httpTransport
						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/SendTripCheckpoint",
								soapEnvelope);
			}
			Object retObj = soapEnvelope.bodyIn;
			if (retObj instanceof SoapFault) {
				SoapFault fault = (SoapFault) retObj;
				Exception ex = new WebServiceException(fault);
				if (eventHandler != null)
					eventHandler.Wsdl2CodeFinishedWithException(ex,checkpointData);
			} else {
				SoapObject result = (SoapObject) retObj;
				if (result.getPropertyCount() > 0) {
					Object obj = result.getProperty(0);
					SoapPrimitive j = (SoapPrimitive) obj;
					String value = (String) j.getValue();
					return Integer.parseInt(value);

				}
			}
		} catch (Exception e) {
			eventHandler.Wsdl2CodeFinishedWithException(new ConnectionException(e), checkpointData);
		}
		return null;
	}

//	public void GetTripCheckpointsAsync(Trip trip,
//			AuthenticationInfo authenticationInfo) throws Exception {
//		if (this.eventHandler == null)
//			throw new Exception("Async Methods Requires IWsdl2CodeEvents");
//		GetTripCheckpointsAsync(trip, authenticationInfo, null);
//	}
//
//	public void GetTripCheckpointsAsync(final Trip trip,
//			final AuthenticationInfo authenticationInfo,
//			final List<HeaderProperty> headers) throws Exception {
//
//		new AsyncTask<Void, Void, VectorCheckpoint>() {
//			@Override
//			protected void onPreExecute() {
//				eventHandler.Wsdl2CodeStartedRequest();
//			};
//
//			@Override
//			protected VectorCheckpoint doInBackground(Void... params) {
//				return GetTripCheckpoints(trip, authenticationInfo, headers);
//			}
//
//			@Override
//			protected void onPostExecute(VectorCheckpoint result) {
//				eventHandler.Wsdl2CodeEndedRequest();
//				if (result != null) {
//					eventHandler
//							.Wsdl2CodeFinished("GetTripCheckpoints", result);
//				}
//			}
//		}.execute();
//	}
//
//	public VectorCheckpoint GetTripCheckpoints(Trip trip,
//			AuthenticationInfo authenticationInfo) {
//		return GetTripCheckpoints(trip, authenticationInfo, null);
//	}
//
//	public VectorCheckpoint GetTripCheckpoints(Trip trip,
//			AuthenticationInfo authenticationInfo, List<HeaderProperty> headers) {
//		SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(
//				SoapEnvelope.VER11);
//		soapEnvelope.implicitTypes = true;
//		soapEnvelope.dotNet = true;
//		SoapObject soapReq = new SoapObject("http://gdzie-jacht.pl/webservice",
//				"GetTripCheckpoints");
//		soapEnvelope.addMapping("http://gdzie-jacht.pl/webservice", "trip",
//				new Trip().getClass());
//		soapEnvelope.addMapping("http://gdzie-jacht.pl/webservice",
//				"authenticationInfo", new AuthenticationInfo().getClass());
//		soapReq.addProperty("trip", trip);
//		soapReq.addProperty("authenticationInfo", authenticationInfo);
//		soapEnvelope.setOutputSoapObject(soapReq);
//		HttpTransportSE httpTransport = new HttpTransportSE(url, timeOut);
//		try {
//			if (headers != null) {
//				httpTransport
//						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/GetTripCheckpoints",
//								soapEnvelope, headers);
//			} else {
//				httpTransport
//						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/GetTripCheckpoints",
//								soapEnvelope);
//			}
//			Object retObj = soapEnvelope.bodyIn;
//			if (retObj instanceof SoapFault) {
//				SoapFault fault = (SoapFault) retObj;
//				Exception ex = new Exception(fault.faultstring);
//				if (eventHandler != null)
//					eventHandler.Wsdl2CodeFinishedWithException(ex);
//			} else {
//				SoapObject result = (SoapObject) retObj;
//				if (result.getPropertyCount() > 0) {
//					Object obj = result.getProperty(0);
//					SoapObject j = (SoapObject) obj;
//					VectorCheckpoint resultVariable = new VectorCheckpoint(j);
//					return resultVariable;
//				}
//			}
//		} catch (Exception e) {
//			if (eventHandler != null)
//				eventHandler.Wsdl2CodeFinishedWithException(e);
//			e.printStackTrace();
//		}
//		return null;
//	}

	public void SendPhotoAsync(Photo photo,
			AuthenticationInfo authenticationInfo, IWsdl2CodeEvents<Integer, Photo> eventHandler) {
		SendPhotoAsync(photo, authenticationInfo, eventHandler, null);
	}

	public void SendPhotoAsync(final Photo photo,
			final AuthenticationInfo authenticationInfo,
			final IWsdl2CodeEvents<Integer, Photo> eventHandler, final List<HeaderProperty> headers) {

		new AsyncTask<Void, Void, Integer>() {
			@Override
			protected void onPreExecute() {
				eventHandler.Wsdl2CodeStartedRequest();
			};

			@Override
			protected Integer doInBackground(Void... params) {
				return SendPhoto(photo, authenticationInfo, eventHandler, headers);
			}

			@Override
			protected void onPostExecute(Integer result) {
				eventHandler.Wsdl2CodeEndedRequest();
				if (result != null) {
					eventHandler.Wsdl2CodeFinished(result);
				}
			}
		}.execute();
	}

	public Integer SendPhoto(Photo photo, AuthenticationInfo authenticationInfo, IWsdl2CodeEvents<Integer, Photo> eventHandler) {
		Integer i= SendPhoto(photo, authenticationInfo, eventHandler, null);
		if (i != null) {
			eventHandler.Wsdl2CodeFinished(i);
		}
		return i;
	}

	public Integer SendPhoto(Photo photo, AuthenticationInfo authenticationInfo,
			IWsdl2CodeEvents<Integer, Photo> eventHandler, List<HeaderProperty> headers) {
		SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		//MarshalBase64 base64 = new MarshalBase64();
		//base64.register(soapEnvelope);
		soapEnvelope.implicitTypes = true;
		soapEnvelope.dotNet = true;
		SoapObject soapReq = new SoapObject("http://gdzie-jacht.pl/webservice",
				"SendPhoto");
		soapEnvelope.addMapping("http://gdzie-jacht.pl/webservice", "photo",
				new Photo().getClass());
		soapEnvelope.addMapping("http://gdzie-jacht.pl/webservice",
				"authenticationInfo", new AuthenticationInfo().getClass());
		soapReq.addProperty("photo", photo);
		soapReq.addProperty("authenticationInfo", authenticationInfo);
		soapEnvelope.setOutputSoapObject(soapReq);
		HttpTransportSE httpTransport = new HttpTransportSE(url, timeOut);
		httpTransport.debug=true;
		try {
			if (headers != null) {
				httpTransport
						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/SendPhoto",
								soapEnvelope, headers);
			} else {
				httpTransport
						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/SendPhoto",
								soapEnvelope);
			}
			Object retObj = soapEnvelope.bodyIn;
			if (retObj instanceof SoapFault) {
				SoapFault fault = (SoapFault) retObj;
				Exception ex = new WebServiceException(fault);
				if (eventHandler != null)
					eventHandler.Wsdl2CodeFinishedWithException(ex, photo);
			} else {
				SoapObject result = (SoapObject) retObj;
				if (result.getPropertyCount() > 0) {
					Object obj = result.getProperty(0);
					SoapPrimitive j = (SoapPrimitive) obj;
					String value = (String) j.getValue();
					return Integer.parseInt(value);
				}
			}
		} catch (Exception e) {
			if (eventHandler != null)
				eventHandler.Wsdl2CodeFinishedWithException(new ConnectionException(e), photo);
		}
		return null;
	}

//	public void GetFriendsPhotosAsync(int userId, boolean userIdSpecified,
//			AuthenticationInfo authenticationInfo) throws Exception {
//		if (this.eventHandler == null)
//			throw new Exception("Async Methods Requires IWsdl2CodeEvents");
//		GetFriendsPhotosAsync(userId, userIdSpecified, authenticationInfo, null);
//	}
//
//	public void GetFriendsPhotosAsync(final int userId,
//			final boolean userIdSpecified,
//			final AuthenticationInfo authenticationInfo,
//			final List<HeaderProperty> headers) throws Exception {
//
//		new AsyncTask<Void, Void, VectorPhoto>() {
//			@Override
//			protected void onPreExecute() {
//				eventHandler.Wsdl2CodeStartedRequest();
//			};
//
//			@Override
//			protected VectorPhoto doInBackground(Void... params) {
//				return GetFriendsPhotos(userId, userIdSpecified,
//						authenticationInfo, headers);
//			}
//
//			@Override
//			protected void onPostExecute(VectorPhoto result) {
//				eventHandler.Wsdl2CodeEndedRequest();
//				if (result != null) {
//					eventHandler.Wsdl2CodeFinished("GetFriendsPhotos", result);
//				}
//			}
//		}.execute();
//	}
//
//	public VectorPhoto GetFriendsPhotos(int userId, boolean userIdSpecified,
//			AuthenticationInfo authenticationInfo) {
//		return GetFriendsPhotos(userId, userIdSpecified, authenticationInfo,
//				null);
//	}
//
//	public VectorPhoto GetFriendsPhotos(int userId, boolean userIdSpecified,
//			AuthenticationInfo authenticationInfo, List<HeaderProperty> headers) {
//		SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(
//				SoapEnvelope.VER11);
//		soapEnvelope.implicitTypes = true;
//		soapEnvelope.dotNet = true;
//		SoapObject soapReq = new SoapObject("http://gdzie-jacht.pl/webservice",
//				"GetFriendsPhotos");
//		soapEnvelope.addMapping("http://gdzie-jacht.pl/webservice",
//				"authenticationInfo", new AuthenticationInfo().getClass());
//		soapReq.addProperty("userId", userId);
//		soapReq.addProperty("userIdSpecified", userIdSpecified);
//		soapReq.addProperty("authenticationInfo", authenticationInfo);
//		soapEnvelope.setOutputSoapObject(soapReq);
//		HttpTransportSE httpTransport = new HttpTransportSE(url, timeOut);
//		try {
//			if (headers != null) {
//				httpTransport
//						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/GetFriendsPhotos",
//								soapEnvelope, headers);
//			} else {
//				httpTransport
//						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/GetFriendsPhotos",
//								soapEnvelope);
//			}
//			Object retObj = soapEnvelope.bodyIn;
//			if (retObj instanceof SoapFault) {
//				SoapFault fault = (SoapFault) retObj;
//				Exception ex = new Exception(fault.faultstring);
//				if (eventHandler != null)
//					eventHandler.Wsdl2CodeFinishedWithException(ex);
//			} else {
//				SoapObject result = (SoapObject) retObj;
//				if (result.getPropertyCount() > 0) {
//					Object obj = result.getProperty(0);
//					SoapObject j = (SoapObject) obj;
//					VectorPhoto resultVariable = new VectorPhoto(j);
//					return resultVariable;
//				}
//			}
//		} catch (Exception e) {
//			if (eventHandler != null)
//				eventHandler.Wsdl2CodeFinishedWithException(e);
//			e.printStackTrace();
//		}
//		return null;
//	}
//
//	public void GetFriendsTripsAsync(int userId, boolean userIdSpecified,
//			AuthenticationInfo authenticationInfo) throws Exception {
//		if (this.eventHandler == null)
//			throw new Exception("Async Methods Requires IWsdl2CodeEvents");
//		GetFriendsTripsAsync(userId, userIdSpecified, authenticationInfo, null);
//	}
//
//	public void GetFriendsTripsAsync(final int userId,
//			final boolean userIdSpecified,
//			final AuthenticationInfo authenticationInfo,
//			final List<HeaderProperty> headers) throws Exception {
//
//		new AsyncTask<Void, Void, VectorTrip>() {
//			@Override
//			protected void onPreExecute() {
//				eventHandler.Wsdl2CodeStartedRequest();
//			};
//
//			@Override
//			protected VectorTrip doInBackground(Void... params) {
//				return GetFriendsTrips(userId, userIdSpecified,
//						authenticationInfo, headers);
//			}
//
//			@Override
//			protected void onPostExecute(VectorTrip result) {
//				eventHandler.Wsdl2CodeEndedRequest();
//				if (result != null) {
//					eventHandler.Wsdl2CodeFinished("GetFriendsTrips", result);
//				}
//			}
//		}.execute();
//	}
//
//	public VectorTrip GetFriendsTrips(int userId, boolean userIdSpecified,
//			AuthenticationInfo authenticationInfo) {
//		return GetFriendsTrips(userId, userIdSpecified, authenticationInfo,
//				null);
//	}
//
//	public VectorTrip GetFriendsTrips(int userId, boolean userIdSpecified,
//			AuthenticationInfo authenticationInfo, List<HeaderProperty> headers) {
//		SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(
//				SoapEnvelope.VER11);
//		soapEnvelope.implicitTypes = true;
//		soapEnvelope.dotNet = true;
//		SoapObject soapReq = new SoapObject("http://gdzie-jacht.pl/webservice",
//				"GetFriendsTrips");
//		soapEnvelope.addMapping("http://gdzie-jacht.pl/webservice",
//				"authenticationInfo", new AuthenticationInfo().getClass());
//		soapReq.addProperty("userId", userId);
//		soapReq.addProperty("userIdSpecified", userIdSpecified);
//		soapReq.addProperty("authenticationInfo", authenticationInfo);
//		soapEnvelope.setOutputSoapObject(soapReq);
//		HttpTransportSE httpTransport = new HttpTransportSE(url, timeOut);
//		try {
//			if (headers != null) {
//				httpTransport
//						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/GetFriendsTrips",
//								soapEnvelope, headers);
//			} else {
//				httpTransport
//						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/GetFriendsTrips",
//								soapEnvelope);
//			}
//			Object retObj = soapEnvelope.bodyIn;
//			if (retObj instanceof SoapFault) {
//				SoapFault fault = (SoapFault) retObj;
//				Exception ex = new Exception(fault.faultstring);
//				if (eventHandler != null)
//					eventHandler.Wsdl2CodeFinishedWithException(ex);
//			} else {
//				SoapObject result = (SoapObject) retObj;
//				if (result.getPropertyCount() > 0) {
//					Object obj = result.getProperty(0);
//					SoapObject j = (SoapObject) obj;
//					VectorTrip resultVariable = new VectorTrip(j);
//					return resultVariable;
//				}
//			}
//		} catch (Exception e) {
//			if (eventHandler != null)
//				eventHandler.Wsdl2CodeFinishedWithException(e);
//			e.printStackTrace();
//		}
//		return null;
//	}
//
//	public void GetTripListForUserAsync(int userId, boolean userIdSpecified,
//			AuthenticationInfo authenticationInfo) throws Exception {
//		if (this.eventHandler == null)
//			throw new Exception("Async Methods Requires IWsdl2CodeEvents");
//		GetTripListForUserAsync(userId, userIdSpecified, authenticationInfo,
//				null);
//	}
//
//	public void GetTripListForUserAsync(final int userId,
//			final boolean userIdSpecified,
//			final AuthenticationInfo authenticationInfo,
//			final List<HeaderProperty> headers) throws Exception {
//
//		new AsyncTask<Void, Void, VectorTrip>() {
//			@Override
//			protected void onPreExecute() {
//				eventHandler.Wsdl2CodeStartedRequest();
//			};
//
//			@Override
//			protected VectorTrip doInBackground(Void... params) {
//				return GetTripListForUser(userId, userIdSpecified,
//						authenticationInfo, headers);
//			}
//
//			@Override
//			protected void onPostExecute(VectorTrip result) {
//				eventHandler.Wsdl2CodeEndedRequest();
//				if (result != null) {
//					eventHandler
//							.Wsdl2CodeFinished("GetTripListForUser", result);
//				}
//			}
//		}.execute();
//	}
//
//	public VectorTrip GetTripListForUser(int userId, boolean userIdSpecified,
//			AuthenticationInfo authenticationInfo) {
//		return GetTripListForUser(userId, userIdSpecified, authenticationInfo,
//				null);
//	}
//
//	public VectorTrip GetTripListForUser(int userId, boolean userIdSpecified,
//			AuthenticationInfo authenticationInfo, List<HeaderProperty> headers) {
//		SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(
//				SoapEnvelope.VER11);
//		soapEnvelope.implicitTypes = true;
//		soapEnvelope.dotNet = true;
//		SoapObject soapReq = new SoapObject("http://gdzie-jacht.pl/webservice",
//				"GetTripListForUser");
//		soapEnvelope.addMapping("http://gdzie-jacht.pl/webservice",
//				"authenticationInfo", new AuthenticationInfo().getClass());
//		soapReq.addProperty("userId", userId);
//		soapReq.addProperty("userIdSpecified", userIdSpecified);
//		soapReq.addProperty("authenticationInfo", authenticationInfo);
//		soapEnvelope.setOutputSoapObject(soapReq);
//		HttpTransportSE httpTransport = new HttpTransportSE(url, timeOut);
//		try {
//			if (headers != null) {
//				httpTransport
//						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/GetTripListForUser",
//								soapEnvelope, headers);
//			} else {
//				httpTransport
//						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/GetTripListForUser",
//								soapEnvelope);
//			}
//			Object retObj = soapEnvelope.bodyIn;
//			if (retObj instanceof SoapFault) {
//				SoapFault fault = (SoapFault) retObj;
//				Exception ex = new Exception(fault.faultstring);
//				if (eventHandler != null)
//					eventHandler.Wsdl2CodeFinishedWithException(ex);
//			} else {
//				SoapObject result = (SoapObject) retObj;
//				if (result.getPropertyCount() > 0) {
//					Object obj = result.getProperty(0);
//					SoapObject j = (SoapObject) obj;
//					VectorTrip resultVariable = new VectorTrip(j);
//					return resultVariable;
//				}
//			}
//		} catch (Exception e) {
//			if (eventHandler != null)
//				eventHandler.Wsdl2CodeFinishedWithException(e);
//			e.printStackTrace();
//		}
//		return null;
//	}
//
//	public void GetTripListAsync(int userId, boolean userIdSpecified,
//			AuthenticationInfo authenticationInfo) throws Exception {
//		if (this.eventHandler == null)
//			throw new Exception("Async Methods Requires IWsdl2CodeEvents");
//		GetTripListAsync(userId, userIdSpecified, authenticationInfo, null);
//	}
//
//	public void GetTripListAsync(final int userId,
//			final boolean userIdSpecified,
//			final AuthenticationInfo authenticationInfo,
//			final List<HeaderProperty> headers) throws Exception {
//
//		new AsyncTask<Void, Void, VectorTrip>() {
//			@Override
//			protected void onPreExecute() {
//				eventHandler.Wsdl2CodeStartedRequest();
//			};
//
//			@Override
//			protected VectorTrip doInBackground(Void... params) {
//				return GetTripList(userId, userIdSpecified, authenticationInfo,
//						headers);
//			}
//
//			@Override
//			protected void onPostExecute(VectorTrip result) {
//				eventHandler.Wsdl2CodeEndedRequest();
//				if (result != null) {
//					eventHandler.Wsdl2CodeFinished("GetTripList", result);
//				}
//			}
//		}.execute();
//	}

//	public VectorTrip GetTripList(int userId, boolean userIdSpecified,
//			AuthenticationInfo authenticationInfo) {
//		return GetTripList(userId, userIdSpecified, authenticationInfo, null);
//	}
//
//	public VectorTrip GetTripList(int userId, boolean userIdSpecified,
//			AuthenticationInfo authenticationInfo, List<HeaderProperty> headers) {
//		SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(
//				SoapEnvelope.VER11);
//		soapEnvelope.implicitTypes = true;
//		soapEnvelope.dotNet = true;
//		SoapObject soapReq = new SoapObject("http://gdzie-jacht.pl/webservice",
//				"GetTripList");
//		soapEnvelope.addMapping("http://gdzie-jacht.pl/webservice",
//				"authenticationInfo", new AuthenticationInfo().getClass());
//		soapReq.addProperty("userId", userId);
//		soapReq.addProperty("userIdSpecified", userIdSpecified);
//		soapReq.addProperty("authenticationInfo", authenticationInfo);
//		soapEnvelope.setOutputSoapObject(soapReq);
//		HttpTransportSE httpTransport = new HttpTransportSE(url, timeOut);
//		try {
//			if (headers != null) {
//				httpTransport
//						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/GetTripList",
//								soapEnvelope, headers);
//			} else {
//				httpTransport
//						.call("http://gdzie-jacht.pl/webservice/IGdzieJachtService/GetTripList",
//								soapEnvelope);
//			}
//			Object retObj = soapEnvelope.bodyIn;
//			if (retObj instanceof SoapFault) {
//				SoapFault fault = (SoapFault) retObj;
//				Exception ex = new Exception(fault.faultstring);
//				if (eventHandler != null)
//					eventHandler.Wsdl2CodeFinishedWithException(ex);
//			} else {
//				SoapObject result = (SoapObject) retObj;
//				if (result.getPropertyCount() > 0) {
//					Object obj = result.getProperty(0);
//					SoapObject j = (SoapObject) obj;
//					VectorTrip resultVariable = new VectorTrip(j);
//					return resultVariable;
//				}
//			}
//		} catch (Exception e) {
//			if (eventHandler != null)
//				eventHandler.Wsdl2CodeFinishedWithException(e);
//			e.printStackTrace();
//		}
//		return null;
//	}

}
