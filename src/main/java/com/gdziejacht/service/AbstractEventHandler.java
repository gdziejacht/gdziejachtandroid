package com.gdziejacht.service;

import com.gdziejacht.PseudoContainer;
import com.gdziejacht.contracts.IErrorHandler;
import com.gdziejacht.service.internal.IWsdl2CodeEvents;

abstract class AbstractEventHandler<ReturnType, InputType> implements IWsdl2CodeEvents<ReturnType, InputType> {
	IErrorHandler errorHandler = PseudoContainer.Instance.getSingleton(IErrorHandler.class);
	public void Wsdl2CodeStartedRequest() {
	}

	public abstract void Wsdl2CodeFinished(ReturnType data);

	public void Wsdl2CodeFinishedWithException(Exception ex, InputType data) {
		errorHandler.handleExceptionAsync(this, ex);
	}

	public void Wsdl2CodeEndedRequest() {
		errorHandler.handle();
	}
}