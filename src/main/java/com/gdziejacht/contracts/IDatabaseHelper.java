package com.gdziejacht.contracts;

import java.util.List;

import com.gdziejacht.DataModels.LocalCheckpoint;
import com.gdziejacht.DataModels.LocalPhoto;
import com.gdziejacht.DataModels.LocalTrip;
import com.gdziejacht.DataModels.LocalUser;


public interface IDatabaseHelper {

	List<LocalTrip> getAllTrips();

	void createTrip(LocalTrip actualTrip);

	void updateTrip(LocalTrip actualTrip);

	void createCheckpoint(LocalCheckpoint actualCheckpoint);

	void updateCheckpoint(LocalCheckpoint localCheckpoint);

	LocalTrip getTrip(LocalTrip actualTrip);

	void deleteTrip(LocalTrip actualTrip);

	void createUser(LocalUser actualUser);

	void deleteUser(LocalUser actualUser);
	void updateUser(LocalUser actualUser);

	List<LocalUser> getAllUsers();

	LocalUser findUserById(LocalUser actualUser);



	List<LocalTrip> getAllTripsByUser(LocalUser user);

	void createPhoto(LocalPhoto actualPhoto);

	List<LocalPhoto> getAllPhotosByTrip(LocalTrip user);

	List<LocalPhoto> getAllPhotos();

	void updatePhoto(LocalPhoto localPhoto);

	void deletePhoto(LocalPhoto actualPhoto);

	LocalCheckpoint getCheckpoint(LocalCheckpoint checkpoint);
	

}
