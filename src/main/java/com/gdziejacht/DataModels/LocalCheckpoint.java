package com.gdziejacht.DataModels;

import android.location.Location;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.gdziejacht.PseudoContainer;
import com.gdziejacht.contracts.IDatabaseHelper;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
@DatabaseTable
public class LocalCheckpoint {
	public static final String FK_LOCALTRIP = "LOCALTRIP";
	static IDatabaseHelper databaseHelper = PseudoContainer.Instance.getSingleton(IDatabaseHelper.class);
	@DatabaseField(generatedId=true)
	int genId;
	@DatabaseField
	int id;
	@DatabaseField
	double lat;
	@DatabaseField
	double lng;
	@DatabaseField
	double alt;
	@DatabaseField
	double speed;
	@DatabaseField
	long time;//set as UTC
	@DatabaseField(foreign=true, columnName=FK_LOCALTRIP)
	LocalTrip trip;
	//to prevent double sending
	boolean sendLock=false;
	public boolean tryLock(){
		boolean oldValue=sendLock;
		sendLock=true;
		return oldValue;
	};
	public boolean isLocked(){
		return sendLock;
	};
	public LocalTrip getTrip() {
		return trip;
	}
	public void setTrip(LocalTrip trip) {
		this.trip = trip;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}
	public double getAlt() {
		return alt;
	}
	public void setAlt(double alt) {
		this.alt = alt;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public DateTime getDateTime(){
		return new DateTime(time, DateTimeZone.UTC);
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
		databaseHelper.updateCheckpoint(this);
	}
    public double distanceTo(LocalCheckpoint other)
    {
        Location location = new Location("");
        location.setLatitude(lat);
        location.setLongitude(lng);

        Location otherLocation = new Location("");
        otherLocation.setLatitude(other.lat);
        otherLocation.setLongitude(other.lng);

        return location.distanceTo(otherLocation);
    }
	public boolean isSend() {
		return id!=0;
	}
}
