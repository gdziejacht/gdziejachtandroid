package com.gdziejacht.errorHandling;


import java.util.LinkedList;
import java.util.Queue;

import android.content.Context;
import android.database.SQLException;
import android.util.Log;
import android.widget.Toast;

import com.gdziejacht.contracts.IErrorHandler;
import com.gdziejacht.contracts.IoC.Autowired;

public class ErrorHandler implements IErrorHandler{
	Queue<Exception> errorList = new LinkedList<>();
	@Autowired
	public void setContext(Context context)
	{
		this.context=context;
	}
	Context context;
	@Override
	public synchronized void handleException(Object source, Exception ex) {
		handleLogic(source, ex, false);
	}
	@Override
	public synchronized void handleExceptionAsync(Object source, Exception ex) {
		handleLogic(source, ex, true);
	}
	private void handleLogic(Object source, Exception ex, boolean async){
		if(ex instanceof SQLException)
			Log.e(source.getClass().getName(), ex.getMessage(), ex);
		else if(ex instanceof WebServiceException){
			if(!((WebServiceException)ex).internal)
				pushOnScreen(ex, async);//show only noninternal errors
		}
		else if(ex instanceof ConnectionException)
			pushOnScreen(new Exception("Aplikacja wymaga połączenia z internetem!"), async);
		else
			pushOnScreen(ex, async);
	}
	@Override
	public void handle() {
		if(context!=null)
		{
		for(int i=0; i<errorList.size();i++)
			pushOnScreen(errorList.poll(), false);
		}
	}
	private void pushOnScreen(Exception ex, boolean async){
		if(async)
			errorList.add(ex);
		else
			Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
	}
}
