package com.gdziejacht.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.gdziejacht.PseudoContainer;
import com.gdziejacht.DataModels.LocalPhoto;
import com.gdziejacht.contracts.IState;
import com.gdziejacht.contracts.IWebServiceConnector;

import java.util.List;

public class LocalPhotoArrayAdapter extends ArrayAdapter<LocalPhoto> {

	private int resource;
	private LayoutInflater inflater;
	private Context context;
	private IWebServiceConnector connector = PseudoContainer.Instance
			.getSingleton(IWebServiceConnector.class);
	private IState state = PseudoContainer.Instance
			.getSingleton(IState.class);
	public LocalPhotoArrayAdapter(Context ctx, int resourceId,
			List<LocalPhoto> objects) {

		super(ctx, resourceId, objects);
		resource = resourceId;
		inflater = LayoutInflater.from(ctx);
		context = ctx;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final LocalPhoto localPhoto = getItem(position);
		LocalPhotoListViewCache cache;

		if (convertView == null) {
			convertView = (RelativeLayout) inflater.inflate(resource, null);
			cache = new LocalPhotoListViewCache(convertView);
			convertView.setTag(cache);
		} else {
			convertView = (RelativeLayout) convertView;
			cache = (LocalPhotoListViewCache) convertView.getTag();
		}

		TextView txtName1 = cache.getText1View(resource);
		txtName1.setText(localPhoto.getShootTimeFormattedString());

		final Button button = cache.getButton(resource);
		if (localPhoto.isSend() || localPhoto.isLocked() || state.getLoggedUser()==null)
			button.setVisibility(Button.INVISIBLE);
		else {
			button.setVisibility(Button.VISIBLE);
			button.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					connector.sendPhoto(localPhoto);
					button.setVisibility(Button.INVISIBLE);
				}
			});
		}

		ImageView imageCity = cache.getImageView(resource);
		imageCity.setImageBitmap(localPhoto.getPhotoBitmap()
				.getSampledSizePhoto(convertView.getMeasuredWidth(),
						convertView.getMeasuredHeight()));
		return convertView;
	}

}
