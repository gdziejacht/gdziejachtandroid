package com.gdziejacht.service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.gdziejacht.DataModels.LocalCheckpoint;
import com.gdziejacht.DataModels.LocalPhoto;
import com.gdziejacht.DataModels.LocalTrip;
import com.gdziejacht.DataModels.LocalUser;
import com.gdziejacht.DataModels.RegisterData;
import com.gdziejacht.commons.ThreadHelper;
import com.gdziejacht.contracts.IState;
import com.gdziejacht.contracts.IWebServiceConnector;
import com.gdziejacht.contracts.IoC.Autowired;
import com.gdziejacht.service.internal.AuthenticationInfo;
import com.gdziejacht.service.internal.Checkpoint;
import com.gdziejacht.service.internal.GdzieJachtService;
import com.gdziejacht.service.internal.Photo;
import com.gdziejacht.service.internal.RegisterContract;
import com.gdziejacht.service.internal.Trip;
import com.gdziejacht.service.internal.User;
import com.gdziejacht.service.internal.VectorByte;

/**
 * @author globalbus WebService compatibility layer
 */
public class WebServiceConnector implements IWebServiceConnector {
	private IState state;
	@Autowired
	public void setState(IState state) {
		this.state = state;
	}

	GdzieJachtService service = new GdzieJachtService();
	AuthenticationInfo auth = new AuthenticationInfo();
	private List<LocalCheckpoint> checkpointsToSend = new ArrayList<>();
	private AsyncPhotoSender asyncPhotoSender;

	public WebServiceConnector() {
		auth.username = "test";
		auth.password = "test";
		asyncPhotoSender = new AsyncPhotoSender();
	}

	private boolean checkLogin() {
		if (state.getLoggedUser() == null) {
			// TODO change to system action
			// errorHandler.handleException(this, new
			// Exception("You must be logged first"));
			// errorHandler.handle();
			return false;
		}
		return true;
	}

	public void login(String login, String password) {
		service.GetUserAsync(login, password, auth,
				new AbstractEventHandler<User, Object>() {
					@Override
					public void Wsdl2CodeFinished(User data) {
						afterLogin(data);
					}
				});
	}

	void afterLogin(User loggedUser) {
		LocalUser localuser = new LocalUser();
		localuser.setId(loggedUser.user_id);
		localuser.setName(loggedUser.user_name);
		localuser.setEmail(loggedUser.user_email);
		state.setLoggedUser(localuser);
	}

	@Override
	public void register(RegisterData data) {
		RegisterContract RegisterContract = new RegisterContract();
		RegisterContract.login = data.login;
		RegisterContract.password = data.password;
		RegisterContract.email = data.email;
		service.RegisterAsync(RegisterContract,
				new AbstractEventHandler<User, RegisterContract>() {
					@Override
					public void Wsdl2CodeFinished(User data) {
						afterLogin(data);
					}
				});

	}

	@Override
	public void BeginTrip(final LocalTrip localTrip) {
		if (checkLogin() && localTrip != null) {
			Trip newTrip = new Trip();
			newTrip.trip_dateSpecified = true;
			newTrip.trip_date = TimeHelper.getxsdDateTime(localTrip
					.getStartTime());
			newTrip.trip_info = localTrip.getInfo();
			newTrip.user_idSpecified = true;
			newTrip.user_id = localTrip.getUser().getId();

			service.BeginTripAsync(newTrip, auth,
					new AbstractEventHandler<Integer, Trip>() {
						@Override
						public void Wsdl2CodeFinished(Integer data) {
							localTrip.setId(data);
							flushCheckpoints();
						}
					});

		}
	}

	private void flushCheckpoints() {
		// TODO it's really too naive method...
		for (LocalCheckpoint i : checkpointsToSend.toArray(new LocalCheckpoint[0])) {
			//toArray to avoid synchronization errors
			sendCheckpoint(i);
		}
		checkpointsToSend.clear();

	}

	@Override
	public void sendCheckpoint(final LocalCheckpoint checkpoint) {
		if (checkLogin()) {
			if(checkpoint.getTrip().getId()==0)
			//if (state.getActualTrip().getId() == 0)
				checkpointsToSend.add(checkpoint);
			else {
				if(checkpoint.tryLock())
					return;//its locked and probably already sending
				Checkpoint checkpointData = new Checkpoint();
				checkpointData.checkpoint_latitudeSpecified = true;
				checkpointData.checkpoint_latitude = checkpoint.getLat();
				checkpointData.checkpoint_longitudeSpecified = true;
				checkpointData.checkpoint_longitude = checkpoint.getLng();
				checkpointData.checkpoint_heightSpecified = true;
				checkpointData.checkpoint_height = checkpoint.getAlt();
				checkpointData.checkpoint_speedSpecified = true;
				checkpointData.checkpoint_speed = checkpoint.getSpeed();
				checkpointData.checkpoint_timestampSpecified = true;
				checkpointData.checkpoint_timestamp = TimeHelper
						.getxsdDateTime(checkpoint.getDateTime());
				checkpointData.trip_idSpecified = true;
				checkpointData.trip_id = checkpoint.getTrip().getId();
				service.SendTripCheckpointAsync(state.getLoggedUser().getId(),
						true, checkpointData, auth,
						new AbstractEventHandler<Integer, Checkpoint>() {
							@Override
							public void Wsdl2CodeFinishedWithException(
									Exception ex, Checkpoint data) {
								super.Wsdl2CodeFinishedWithException(ex, data);
								checkpointsToSend.add(checkpoint);// add to
																	// queue
							}

							@Override
							public void Wsdl2CodeFinished(Integer data) {
								checkpoint.setId(data);
								flushCheckpoints();
							}
						});
			}
		}
	}

	@Override
	public void sendPhoto(final LocalPhoto localPhoto) {
		asyncPhotoSender.addToQueue(localPhoto);
		asyncPhotoSender.flush();
	}

	class AsyncPhotoSender implements Runnable {
		Lock lock = new ReentrantLock();// its something like monitor, but
										// better
		private LinkedList<LocalPhoto> queue = new LinkedList<>();

		public void addToQueue(final LocalPhoto localPhoto) {
			synchronized (queue) {
				queue.add(localPhoto);
			}

		}

		public void flush() {
			if (lock.tryLock()) {
				try {
					ThreadHelper.launchInBackground(this);
				} finally {
					lock.unlock();
				}
			}
		}

		public void run() {
			try {
				lock.lock();
				if (!checkLogin())// check another one
					return;
				final LocalPhoto localPhoto;
				synchronized (queue) {
					if (!queue.isEmpty())
						localPhoto = queue.pop();
					else
						localPhoto = null;
				}
				if (localPhoto == null || localPhoto.getId()!=0)//non exists or already send
					return;
				if (localPhoto.getCheckpoint().getId() == 0) {
					synchronized (queue) {
						queue.add(localPhoto);
					}
					return;
				}
				if(localPhoto.tryLock())
					return;//its locked and probably already sending
				byte[] fileData = localPhoto.getFileData();
				if (fileData == null)
					return;// something is really, really wrong
				VectorByte data = new VectorByte(fileData);// wrap object
				final Photo photo = new Photo();
				photo.checkpoint_idSpecified = true;
				photo.checkpoint_id = localPhoto.getCheckpoint().getId();
				photo.photo_filename = localPhoto.getFilenameOnly();
				photo.user_idSpecified = true;
				photo.user_id = state.getLoggedUser().getId();
				photo.photo_file = data;
				service.SendPhoto(photo, auth,
						new AbstractEventHandler<Integer, Photo>() {
							@Override
							public void Wsdl2CodeFinished(Integer data) {
								localPhoto.setId(data);
							}
						});
				ThreadHelper.launchInBackground(this);
			} finally {
				lock.unlock();
			}
		}
	}
}
