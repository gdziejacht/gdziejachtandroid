package com.gdziejacht.contracts.IoC;

import android.content.Context;


public interface IContainer {

	void putSingleton(Object obj);

	<T> T getSingleton(Class<? extends T> clazz);

	//void reload();

	//void dispose();

	void putSingleton(Object obj, boolean resolve);


	void putSingleton(Object obj, Class<?> clazz, boolean b);

	//void savePersistenceClasses();

	//void savePersistenceClasses(FileOutputStream fos) throws IOException;

	void registerApp(Context applicationContext);

}
