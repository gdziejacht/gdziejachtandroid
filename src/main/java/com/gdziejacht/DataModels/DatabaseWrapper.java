package com.gdziejacht.DataModels;

import android.content.Context;
import com.gdziejacht.commons.ThreadHelper;
import com.gdziejacht.contracts.IDatabaseHelper;
import com.gdziejacht.contracts.IErrorHandler;
import com.gdziejacht.contracts.IoC.Autowired;
import com.gdziejacht.contracts.IoC.IActivate;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.util.List;

public class DatabaseWrapper implements IDatabaseHelper, IActivate {
	private Context context;
	private IErrorHandler errorHandler;

	@Override
	public void activate() {
		//to do faster database connection
		ThreadHelper.launchInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					helper.getLocalTripDao();
					helper.getLocalCheckpointDao();
					helper.getLocalPhotoDao();
					helper.getLocalUserDao();
				} catch (SQLException e) {
					errorHandler.handleException(this, e);
				}

			}
		});

	}

	@Autowired
	public void setContext(Context context) {
		this.context = context;
		this.helper = new DatabaseHelper(context);
	}

	@Autowired
	public void setErrorHandler(IErrorHandler errorHandler) {
		this.errorHandler = errorHandler;
	}

	DatabaseHelper helper;

	@Override
	public List<LocalTrip> getAllTrips() {
		try {
			return helper.getLocalTripDao().queryForAll();
		} catch (SQLException e) {
			errorHandler.handleException(this, e);
		}
		return null;

	}
	@Override
	public List<LocalTrip> getAllTripsByUser(LocalUser user) {
		//NOT TESTED
		try {
			QueryBuilder<LocalTrip, Integer> query = helper.getLocalTripDao().queryBuilder();
			//get all trips by user and trips unassigned to any user
			Where<LocalTrip, Integer> where = query.where();
			where.isNull(LocalTrip.FK_LOCALUSER);
			if(user!=null)
				where.or().eq(LocalTrip.FK_LOCALUSER, user.getGenId());
			List<LocalTrip> trips = query.query();
			return trips;
		} catch (SQLException e) {
			errorHandler.handleException(this, e);
		}
		return null;

	}
	@Override
	public void createTrip(LocalTrip actualTrip) {
		try {
			helper.getLocalTripDao().create(actualTrip);
		} catch (SQLException e) {
			errorHandler.handleException(this, e);
		}

	}

	@Override
	public void updateTrip(LocalTrip actualTrip) {
		try {
			helper.getLocalTripDao().update(actualTrip);
		} catch (SQLException e) {
			errorHandler.handleException(this, e);
		}
	}
	
	@Override
	public void deleteTrip(LocalTrip actualTrip) {
		try {
			actualTrip.deleteAllCheckpoints();
			helper.getLocalTripDao().delete(actualTrip);
		} catch (SQLException e) {
			errorHandler.handleException(this, e);
		}
	}
	
	@Override
	public void createCheckpoint(LocalCheckpoint actualCheckpoint) {
		try {
			helper.getLocalCheckpointDao().create(actualCheckpoint);
		} catch (SQLException e) {
			errorHandler.handleException(this, e);
		}
	}

	@Override
	public void updateCheckpoint(LocalCheckpoint actualCheckpoint) {
		try {
			helper.getLocalCheckpointDao().update(actualCheckpoint);
		} catch (SQLException e) {
			errorHandler.handleException(this, e);
		}
	}

	@Override
	public LocalTrip getTrip(LocalTrip actualTrip) {
		try {
			return helper.getLocalTripDao().queryForSameId(actualTrip);
		} catch (SQLException e) {
			errorHandler.handleException(this, e);
		}
		return actualTrip;
	}
	@Override
	public List<LocalUser> getAllUsers() {
		try {
			return helper.getLocalUserDao().queryForAll();
		} catch (SQLException e) {
			errorHandler.handleException(this, e);
		}
		return null;

	}
	@Override
	public LocalUser findUserById(LocalUser actualUser) {
		//NOT TESTED
		try {
			QueryBuilder<LocalUser, Integer> query = helper.getLocalUserDao().queryBuilder();
			query.where().eq(LocalUser.ID_LOCALUSER, actualUser.getId());
			return query.queryForFirst();
		} catch (SQLException e) {
			errorHandler.handleException(this, e);
		}
		return null;
	}
	@Override
	public void createUser(LocalUser actualUser) {
		try {
			helper.getLocalUserDao().create(actualUser);
		} catch (SQLException e) {
			errorHandler.handleException(this, e);
		}
	}
	@Override
	public void deleteUser(LocalUser actualUser) {
		try {
			helper.getLocalUserDao().delete(actualUser);
		} catch (SQLException e) {
			errorHandler.handleException(this, e);
		}
	}
	@Override
	public void updateUser(LocalUser actualUser) {
		try {
			helper.getLocalUserDao().update(actualUser);
		} catch (SQLException e) {
			errorHandler.handleException(this, e);
		}
	}
	@Override
	public void createPhoto(LocalPhoto actualPhoto) {
		try {
			helper.getLocalPhotoDao().create(actualPhoto);
		} catch (SQLException e) {
			errorHandler.handleException(this, e);
		}
	}
	@Override
	public void deletePhoto(LocalPhoto actualPhoto) {
		try {
			helper.getLocalPhotoDao().delete(actualPhoto);
			//TODO we need to delete checkpoints and photos also?
		} catch (SQLException e) {
			errorHandler.handleException(this, e);
		}
	}
	@Override
	public void updatePhoto(LocalPhoto actualPhoto) {
		try {
			helper.getLocalPhotoDao().update(actualPhoto);
		} catch (SQLException e) {
			errorHandler.handleException(this, e);
		}
	}
	@Override
	public List<LocalPhoto> getAllPhotosByTrip(LocalTrip trip) {
		//NOT TESTED
		try {
			if(trip==null)
				return null;
			QueryBuilder<LocalCheckpoint, Integer> queryCheckpoints = helper.getLocalCheckpointDao().queryBuilder();
			//all checkpoints from trip
			queryCheckpoints.where().eq(LocalCheckpoint.FK_LOCALTRIP, trip.genId);
			//get all photos related to this checkpoints
			QueryBuilder<LocalPhoto, Integer> queryPhotos = helper.getLocalPhotoDao().queryBuilder();
			List<LocalPhoto> photos = queryPhotos.join(queryCheckpoints).query();
			return photos;
		} catch (SQLException e) {
			errorHandler.handleException(this, e);
		}
		return null;

	}
	@Override
	public List<LocalPhoto> getAllPhotos() {
		try {
			return helper.getLocalPhotoDao().queryForAll();
		} catch (SQLException e) {
			errorHandler.handleException(this, e);
		}
		return null;

	}

	@Override
	public LocalCheckpoint getCheckpoint(LocalCheckpoint checkpoint) {
		try {
			return helper.getLocalCheckpointDao().queryForSameId(checkpoint);
		} catch (SQLException e) {
			errorHandler.handleException(this, e);
		}
		return checkpoint;
	}
}
