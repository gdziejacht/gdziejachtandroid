package com.gdziejacht.gui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import com.gdziejacht.R;

public class PhotoActivity extends SuperActivity {
    public static String PHOTO_EXTRA_INTENT_NAME="photo_path";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //((ImageView)findViewById(R.id.photo_imageView)).setImageBitmap(null);
        if(getIntent().hasExtra(PHOTO_EXTRA_INTENT_NAME)) {
            Bitmap bitmap = BitmapFactory.decodeFile(getIntent().getStringExtra(PHOTO_EXTRA_INTENT_NAME));
            ImageView imageView = ((ImageView)findViewById(R.id.photo_imageview));
            imageView.setImageBitmap(bitmap);
        }
    }
}
