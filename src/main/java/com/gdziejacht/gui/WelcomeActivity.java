package com.gdziejacht.gui;

import com.gdziejacht.*;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

/**
 * Created by Piotr on 2014-11-08.
 */
public class WelcomeActivity extends SuperActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
//    	if(state.getLoggedUser()!=null)//autologin feature
//    		this.loggedSuccessfully();
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);


        Button loginButton = (Button)findViewById(R.id.btnLogin);
        Button registerButton = (Button)findViewById(R.id.btnRegister);
        Button startButton = (Button)findViewById(R.id.btnStart);
        loginButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
            	WelcomeActivity.this.switchActivity(LoginActivity.class);
            }
        });
        registerButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
            	WelcomeActivity.this.switchActivity(RegisterActivity.class);
            }
        });
        startButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
            	WelcomeActivity.this.switchActivity(RouteInfoActivity.class);
            }
        });
    }
}
