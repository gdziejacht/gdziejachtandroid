package com.gdziejacht.gui;


import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import com.gdziejacht.*;
import com.gdziejacht.DataModels.RegisterData;
import com.gdziejacht.contracts.IState;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Piotr on 2014-11-08.
 */
public class RegisterActivity extends SuperActivity {
    EditText loginInput;
    EditText passInput;
    EditText passRepeatInput;
    EditText emailInput;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        state.addPropertyChangeListener(IState.ACTION_LOGIN, loginListener);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Button confirmButton = (Button)findViewById(R.id.confirm_login);
        loginInput = (EditText)findViewById(R.id.reg_login_input);
        passInput = (EditText)findViewById(R.id.reg_password_input);
        passRepeatInput = (EditText)findViewById(R.id.reg_pass_repeat_input);
        emailInput = (EditText)findViewById(R.id.reg_email_input);
        confirmButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String login = loginInput.getText().toString();
                String password = passInput.getText().toString();
                //TODO check password
                String passRepeat = passRepeatInput.getText().toString();
                String email = emailInput.getText().toString();
                RegisterData data = new RegisterData();
                data.login=login;
                data.password=password;
                data.email=email;
                connector.register(data);
            }
        });
    }
	private PropertyChangeListener loginListener = new PropertyChangeListener() {

		@Override
		public void propertyChange(PropertyChangeEvent event) {
			if(event.getNewValue()!=null){
				RegisterActivity.this.switchActivity(RouteInfoActivity.class);
				RegisterActivity.this.finish();//close activity after register
			}
		}
	};
	@Override
	protected void onDestroy(){
		super.onDestroy();
		state.removePropertyChangeListener(IState.ACTION_LOGIN, loginListener);
	}
	
	
}
