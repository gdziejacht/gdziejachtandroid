package com.gdziejacht;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.joda.time.DateTime;
import org.joda.time.ReadableDateTime;

import android.content.Context;

import com.gdziejacht.DataModels.DatabaseWrapper;
import com.gdziejacht.contracts.IoC.Autowired;
import com.gdziejacht.contracts.IoC.IActivate;
import com.gdziejacht.contracts.IoC.IContainer;
import com.gdziejacht.contracts.IoC.Internal;
import com.gdziejacht.errorHandling.ErrorHandler;
import com.gdziejacht.service.WebServiceConnector;

public class PseudoContainer implements IContainer {
	private Map<String, Object> configs = new HashMap<>();
	private Map<String, Object> resolved = new HashMap<>();
	private HashMap<String, Object> unresolved = new HashMap<>();
	private Context applicationContext;
	// private boolean saveFlag = false;
	public static IContainer Instance;
	private boolean goodState = false;
	static {
		Instance = new PseudoContainer();
	}

	// constructor
	private PseudoContainer() {
		_register();
		//
		// _solveDependencies();
	}

	/**
	 * Some of classes in application may need to do additional actions before
	 * closing. This method should be called instead of calling System.exit()
	 * directly.
	 */
	// @Override
	// public void dispose() {
	// for (Object obj : configs.values()) {
	// if (obj instanceof IDisposable)
	// try {
	// ((IDisposable) obj).deactivate();
	// } catch (Throwable e) {
	// e.printStackTrace();
	// }
	// }
	// //savePersistenceClasses();
	// System.exit(0);
	// }


	/**
	 * @param object
	 *            to register
	 */
	@Override
	public void putSingleton(Object obj) {
		putSingleton(obj, false);
	}

	@Override
	public void putSingleton(Object obj, Class<?> clazz, boolean resolve) {
		configs.put(clazz.getName(), obj);
		unresolved.put(clazz.getName(), obj);
		if (resolve)
			_solveDependencies();
	}

	@Override
	public void putSingleton(Object obj, boolean resolve) {
		Class<?> clazz = obj.getClass();
		if (!clazz.isInterface()) {
			for (Class<?> item : clazz.getInterfaces()) {
				if (!item.isAnnotationPresent(Internal.class)) // internal
				// interfaces
				// should not be
				// registered
				{
					configs.put(item.getName(), obj);
					unresolved.put(item.getName(), obj);
				}
			}
			// if (obj instanceof ISerializable)
			// ((ISerializable)
			// obj).addPropertyChangeListener(changeActionListener);
		}
		if (resolve)
			_solveDependencies();
	}

	/**
	 * getting registered object by providing interface class
	 * 
	 * @return registered object or null if nothing found
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <T> T getSingleton(Class<? extends T> clazz) {
		T result = null;
		try {
			// if (clazz.isInterface()) {
			result = (T) configs.get(clazz.getName());
			// }

		} catch (ClassCastException e) {
			// this simply can't occurs, but...
			Logger.getLogger(Constants.APPNAME).log(Level.SEVERE,
					"Error in Container");
		}
		return result;
	}

	private void _register() {
		// workaround to register itself and provide container functionality via
		// @see Autowired
		putSingleton(this);
		putSingleton(new WebServiceConnector());
		putSingleton(new State());
		putSingleton(new ErrorHandler());
		putSingleton(new DatabaseWrapper());
		
		///Ugly hack to keep joda time classes in memory.
		resolved.put(ReadableDateTime.class.getName(), new DateTime());
	}

	/**
	 * Solve all setter dependencies with
	 * 
	 * @see Autowired annotation for registered dependencies
	 */
	private void _solveDependencies() {
		HashMap<String, Object> unresolved2 = new HashMap<>(unresolved);
		for (Entry<String, Object> unresolvedEntry : unresolved2.entrySet()) {
			if (_solveDependencies(unresolvedEntry.getValue())) {
				resolved.put(unresolvedEntry.getKey(),
						unresolvedEntry.getValue());
				unresolved.remove(unresolvedEntry.getKey());
			}
		}
		for (Object obj : resolved.values())
			if (obj instanceof IActivate)
				((IActivate) obj).activate();

	}

	private boolean _solveDependencies(Object obj) {
		Method[] methods = obj.getClass().getDeclaredMethods();
		Collection<Method> annotatedMethods = new LinkedList<>();
		for (Method method : methods)
			if (method.isAnnotationPresent(Autowired.class))
				annotatedMethods.add(method);
		if (annotatedMethods.size() == 0)
			return true;
		else {
			int i = 0;
			for (Method method : annotatedMethods) {
				i++;
				Class<?>[] params = method.getParameterTypes();
				if (params.length == 1) {
					try {
						Object param = getSingleton(params[0]);
						if (param != null)
							method.invoke(obj, param);
						else
							return false;
						if (i == annotatedMethods.size())
							return true;
					} catch (IllegalAccessException | IllegalArgumentException
							| InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
		}
		return false;

	}

	/**
	 * Context of app is a special case
	 * 
	 * @see
	 */
	@Override
	public void registerApp(Context applicationContext) {
		if (!goodState) {
			this.applicationContext = applicationContext;
			putSingleton(applicationContext, android.content.Context.class,
					false);
			_solveDependencies();
			goodState=true;
		}
	}
}
