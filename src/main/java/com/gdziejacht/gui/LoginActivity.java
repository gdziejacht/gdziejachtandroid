package com.gdziejacht.gui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.gdziejacht.R;
import com.gdziejacht.contracts.IState;

/**
 * Created by Piotr on 2014-11-08.
 */
public class LoginActivity extends SuperActivity {
    EditText loginInput;
    EditText passInput;
    Button logoutButton;
    Button cancelLogoutButton;
    Button confirmButton;
	@Override
    public void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        state.addPropertyChangeListener(IState.ACTION_LOGIN, loginListener);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        confirmButton = (Button)findViewById(R.id.confirm_login);
        logoutButton = (Button)findViewById(R.id.btnLogout);
        cancelLogoutButton = (Button)findViewById(R.id.btnCancelLogout);
        loginInput = (EditText)findViewById(R.id.login_input);
        passInput = (EditText)findViewById(R.id.password_input);
        confirmButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String login = loginInput.getText().toString();
                String password = passInput.getText().toString();
                connector.login(login, password);
            }
        });
        if(state.getLoggedUser()!=null)
        	hideOn();
    }
	private PropertyChangeListener loginListener = new PropertyChangeListener() {

		@Override
		public void propertyChange(PropertyChangeEvent event) {
			if(event.getNewValue()!=null){
				hideOn();//logged in successfully
				LoginActivity.this.switchActivity(RouteInfoActivity.class);
			}
			else
				hideOff();//logged out
		}
	};
	
	private void hideOn(){
		//make all elements on screen inactive and show only button "logoff" with this action
		loginInput.setEnabled(false);
		passInput.setEnabled(false);
		confirmButton.setVisibility(View.GONE);
		logoutButton.setVisibility(View.VISIBLE);
		cancelLogoutButton.setVisibility(View.VISIBLE);
		
		logoutButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
            	state.setLoggedUser(null);
            	loginInput.setText("");
            	passInput.setText("");
            	LoginActivity.this.switchActivity(WelcomeActivity.class);
            }
        });
		
		cancelLogoutButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
            	LoginActivity.this.switchActivity(RouteInfoActivity.class);
            }
        });	
	}
	private void hideOff(){
		//reverse effects of "hide on"
		loginInput.setEnabled(true);
		passInput.setEnabled(true);
		confirmButton.setVisibility(View.VISIBLE);
		logoutButton.setVisibility(View.GONE);
		cancelLogoutButton.setVisibility(View.GONE);
	}
	@Override
	protected void onDestroy(){
		super.onDestroy();
		state.setLoggedUser(null);
		state.removePropertyChangeListener(IState.ACTION_LOGIN, loginListener);
	}
	
    @Override
    public void onBackPressed() {
    	if (state.getLoggedUser() != null) {
    		LoginActivity.this.switchActivity(RouteInfoActivity.class);
    	} else {
    		super.onBackPressed();
    	}
    }
}
