package com.gdziejacht.errorHandling;

@SuppressWarnings("serial")
public class ConnectionException extends Exception {

	public ConnectionException(Exception e) {
		super(e.getMessage(), e.getCause());
	}

}
