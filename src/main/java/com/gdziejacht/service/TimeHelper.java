package com.gdziejacht.service;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public class TimeHelper {
	static DateTimeFormatter formatter = ISODateTimeFormat.dateTimeNoMillis();
	public static String getxsdDateTime(DateTime dateTime) {
		return dateTime.toString(formatter);
	}

}
