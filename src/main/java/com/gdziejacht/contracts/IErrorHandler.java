package com.gdziejacht.contracts;

public interface IErrorHandler {

	void handle();

	void handleException(Object source, Exception ex);

	void handleExceptionAsync(Object source, Exception ex);

}
