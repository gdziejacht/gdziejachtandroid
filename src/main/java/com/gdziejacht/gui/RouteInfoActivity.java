package com.gdziejacht.gui;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gdziejacht.DataModels.LocalTrip;
import com.gdziejacht.R;
import com.gdziejacht.commons.StatisticsHelper;
import com.gdziejacht.contracts.IState;
import com.gdziejacht.location.GpsLocationServiceNew;

import org.joda.time.DateTime;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Piotr on 2014-11-11.
 */
public class RouteInfoActivity extends SuperActivity {
	private Intent service;

	private StatisticsHelper statistics;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.route);

		// localization service startup
		service = new Intent(RouteInfoActivity.this,
				GpsLocationServiceNew.class);
		RouteInfoActivity.this.startService(service);

		state.addPropertyChangeListener(IState.ACTION_POSITION_CHANGED,
				positionListener);
		state.addPropertyChangeListener(IState.ACTION_BEGINTRIP,
				beginTripListener);
		state.addPropertyChangeListener(IState.ACTION_GPSSTATE_CHANGED,
				gpsStateListener);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		Button crossbarButton = (Button) findViewById(R.id.crossbar_button);
		
		Button pinButton = (Button) findViewById(R.id.pin_button);

		crossbarButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				RouteInfoActivity.this.switchActivity(MapActivity.class);
			}
		});
		pinButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				RouteInfoActivity.this.switchActivity(GalleryActivity.class);
			}
		});
		final Button sendRouteButton = (Button) findViewById(R.id.send_route_button);
		sendRouteButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				state.reSendVisibleTrip();
				sendRouteButton.setVisibility(Button.INVISIBLE);// ugly, but we
																// don't need
																// double clicks
			}
		});
		// showArchivalRoutes();
		// setup statistics helper;
		this.statistics = new StatisticsHelper();
		statistics.setTrip(state.getVisibleTrip());
		//pseudo signal to change gps icon
		gpsStateListener.propertyChange(new PropertyChangeEvent(this, IState.ACTION_GPSSTATE_CHANGED, null, state.isGpsEnabled()));
		

	}

	@Override
	protected void onResume() {
		super.onResume();
		showArchivalRoutes();
		// refresh information about track
		positionListener.propertyChange(new PropertyChangeEvent(this,
				IState.ACTION_POSITION_CHANGED, state.getActualCheckpoint(),
				state.getActualCheckpoint()));
	}

	private void showArchivalRoutes() {
		// much more better method to handle archive trips
		if (state.getActualTrip() == state.getVisibleTrip()) {
			TextView archivalRoutesTitle = (TextView) findViewById(R.id.archival_routes_title);
			archivalRoutesTitle.setVisibility(View.VISIBLE);
			LinearLayout tripsListView = (LinearLayout) findViewById(R.id.tripsListView);
			List<LocalTrip> localTrips = state.getAllTrips();
			localTrips.remove(state.getActualTrip());
			localTrips=cleanTrips(localTrips);//perform cleanup on database, without current trip.
			tripsListView.removeAllViews();
			int i = 0;
			
			for (final LocalTrip localTrip : localTrips) {
				i++;
				Button tripButton = new Button(this);
				DateTime startTime = localTrip.getStartTime();
				String title = String.format(
						"%3d.\t%02d.%02d.%04d\t%02d:%02d:%02d", i,
						startTime.getDayOfMonth(), startTime.getMonthOfYear(),
						startTime.getYear(), startTime.getHourOfDay(),
						startTime.getMinuteOfHour(),
						startTime.getSecondOfMinute());
				tripButton.setText(title);
				tripButton.setTextColor(Color.parseColor("#82c1d4"));
				tripButton.setTextSize(20);
				tripButton.setTypeface(Typeface.MONOSPACE);
				tripButton.setBackgroundColor(Color.TRANSPARENT);
				tripButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						switchTrip(localTrip);
					}
				});
				tripsListView.addView(tripButton);

			}
			Button sendRouteButton = (Button) findViewById(R.id.send_route_button);
			sendRouteButton.setVisibility(Button.INVISIBLE);
		} else if (!state.getVisibleTrip().isSend()
				&& state.getLoggedUser() != null&& state.getVisibleTrip().isFinished()) {
			Button sendRouteButton = (Button) findViewById(R.id.send_route_button);
			sendRouteButton.setVisibility(Button.VISIBLE);
		} else {
			Button sendRouteButton = (Button) findViewById(R.id.send_route_button);
			sendRouteButton.setVisibility(Button.INVISIBLE);
		}
	}

	private void switchTrip(LocalTrip trip) {
		state.setVisibleTrip(trip);
		this.switchActivity(RouteInfoActivity.class);// open new RouteInfo and
														// do not close this
	}

	//to prevent async errors on switching trips
	@Override
	public void onBackPressed() {
		state.setVisibleTrip(null);
		super.onBackPressed();
	}

	PropertyChangeListener gpsStateListener = new PropertyChangeListener() {

		@Override
		public void propertyChange(PropertyChangeEvent event) {
			// Make gps state visible on screen
			Button crossbarButton = (Button) findViewById(R.id.crossbar_button);
			int resource = (Boolean) event.getNewValue() ? R.drawable.crossbar_active
					: R.drawable.crossbar_nonactive;
			crossbarButton.setBackgroundResource(resource);
		}
	};

	PropertyChangeListener positionListener = new PropertyChangeListener() {
		@Override
		public void propertyChange(PropertyChangeEvent event) {
			if (state.getActualCheckpoint() != null) {
				statistics.addCheckpoint(state.getActualCheckpoint());
			}
			((TextView) findViewById(R.id.height)).setText(statistics
					.getFormattedHeight());
			((TextView) findViewById(R.id.duration)).setText(statistics
					.getFormattedDuration());
			((TextView) findViewById(R.id.speed)).setText(statistics
					.getFormattedSpeedKMPerH());
			((TextView) findViewById(R.id.distance)).setText(statistics
					.getFormattedDistanceKM());

		}
	};
	PropertyChangeListener beginTripListener = new PropertyChangeListener() {
		@Override
		public void propertyChange(PropertyChangeEvent event) {
			statistics.setTrip(state.getVisibleTrip());

		}
	};
	private List<LocalTrip> cleanTrips(List<LocalTrip> localTrips){
		ArrayList<LocalTrip> cleanedTrips= new ArrayList<>();
		cleanedTrips.ensureCapacity(localTrips.size());
		for (final LocalTrip localTrip : localTrips) {
			if(localTrip.getCheckpoints().length==0){
				state.deleteTrip(localTrip);
				continue;
			}
			if (!localTrip.isFinished())
				localTrip.finish();// force finish improper route or strange
									// things will happen
			cleanedTrips.add(localTrip);
		}
		return cleanedTrips;
	}
}
