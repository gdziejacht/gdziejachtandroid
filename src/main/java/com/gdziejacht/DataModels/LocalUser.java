package com.gdziejacht.DataModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
@DatabaseTable
public class LocalUser {
	public static final String ID_LOCALUSER = "ID";
	@DatabaseField(generatedId=true)
	private int genId;
	@DatabaseField(columnName=ID_LOCALUSER)
	private int id;
	@DatabaseField
	private String name;
	@DatabaseField
	private String email;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getGenId() {
		return genId;
	}
}
