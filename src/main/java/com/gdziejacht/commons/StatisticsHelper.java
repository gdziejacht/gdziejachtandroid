package com.gdziejacht.commons;

import org.joda.time.Duration;
import org.joda.time.format.PeriodFormat;
import org.joda.time.format.PeriodFormatter;

import com.gdziejacht.DataModels.LocalCheckpoint;
import com.gdziejacht.DataModels.LocalTrip;

public class StatisticsHelper {
	private double height = 0;
	private LocalTrip trip;
	private Duration duration;

	public void setTrip(LocalTrip trip) {
		this.trip = trip;
		if (trip != null) {
			distance = 0;
			duration=null;
			speed=0;
			LocalCheckpoint[] checkpoints = trip.getCheckpoints();
			if (checkpoints.length > 0) {
				for (int i = 0; i < checkpoints.length - 1; i++) {
					distance += checkpoints[i].distanceTo(checkpoints[i + 1]);
				}
				duration = new Duration(trip.getLastCheckpoint().getTime()
						- trip.getFirstCheckpoint().getTime());
				if (duration.getMillis() != 0)
					speed = distance / (duration.getMillis()/1000);//meters per second
				else
					speed = 0;
			}
		}
	}

	public LocalTrip getTrip() {
		return trip;
	}

	public double getHeight() {
		return height;
	}
	public String getFormattedHeight() {
		return String.format("%.2f m", height);
	}
	public double getSpeed() {
		return speed;
	}
	public String getFormattedSpeed() {
		return String.format("%.2f m/s", speed);
	}
	public String getFormattedSpeedKMPerH() {
		return String.format("%.2f km/h", speed*3.6);
	}
	public double getDistance() {
		return distance;
	}
	public String getFormattedDistanceKM() {
		return String.format("%.2f KM", distance/1000);
	}

	public String getFormattedDuration() {
		if (duration != null)
			return formatter.print(duration.toPeriod());
		return "";
	}

	private double speed = 0;
	private double distance = 0;
	private PeriodFormatter formatter;
	private LocalCheckpoint previous;

	public StatisticsHelper() {
		formatter = PeriodFormat.wordBased();
	}

	public void addCheckpoint(LocalCheckpoint checkpoint) {
		LocalCheckpoint previous = this.previous;
		this.previous = checkpoint;
		height = checkpoint.getAlt();// it's not related with trip

		if (trip == null)
			return;
		if (!trip.isFinished()&&previous!=null) {
			duration = new Duration(trip.getLastCheckpoint().getTime()
					- trip.getFirstCheckpoint().getTime());
			distance += previous.distanceTo(checkpoint);
			if (duration.getMillis() != 0)
				speed = distance / (duration.getMillis()/1000);//meters per second
			else
				speed = 0;
		}
	}
}
