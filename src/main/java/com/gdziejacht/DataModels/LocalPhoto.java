package com.gdziejacht.DataModels;


import com.gdziejacht.PseudoContainer;
import com.gdziejacht.contracts.IDatabaseHelper;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
@DatabaseTable
public class LocalPhoto {
	static IDatabaseHelper databaseHelper = PseudoContainer.Instance.getSingleton(IDatabaseHelper.class);
	@DatabaseField(generatedId=true)
	int genId;

	@DatabaseField
	boolean wasSend;
	@DatabaseField(foreign=true)
	LocalCheckpoint checkpoint;
    public int getGenId() {
		return genId;
	}
	public void setGenId(int genId) {
		this.genId = genId;
	}
	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}

	public static final String GALLERY_DATE_TIME_FORMATTER_STRING="yyyy-MM-dd HH:mm:ss";
    public static final DateTimeFormatter GALLERY_DATE_TIME_FORMATTER = DateTimeFormat.forPattern(GALLERY_DATE_TIME_FORMATTER_STRING);
    @DatabaseField(persisted=false)
    PhotoBitmap photoBitmap;
    @DatabaseField
    String fullPath;
    @DatabaseField
	int id;//non zero value means - send to server
    boolean loaded=false;
    public LocalPhoto(String fullPath, LocalCheckpoint localCheckpoint) {
        this.fullPath=fullPath;
        this.checkpoint=localCheckpoint;
    }
    public LocalPhoto(){
    	
    };
    public DateTime getShootTime() {
        return getCheckpoint().getDateTime();
    }

    public String getShootTimeFormattedString() {
        return getCheckpoint().getDateTime().toString(GALLERY_DATE_TIME_FORMATTER);
    }

    public String getFullPath() {
        return fullPath;
    }
    public String getFilenameOnly() {
    	int index = fullPath.lastIndexOf('/')+1;
    	int lenght=fullPath.length()-index-1;
    	if(lenght<50)
    		return fullPath.substring(index, fullPath.length());
    	else
    		return fullPath.substring(index, index+50);
    }
	public LocalCheckpoint getCheckpoint() {
		if(!loaded)
			checkpoint=databaseHelper.getCheckpoint(checkpoint);
		return checkpoint;
	}
	public void setCheckpoint(LocalCheckpoint checkpoint) {
		this.checkpoint = checkpoint;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id=id;
		databaseHelper.updatePhoto(this);
	}
    public byte[] getFileData() {
        return getPhotoBitmap().getFileData();
    }
    public PhotoBitmap getPhotoBitmap() {
    	if(photoBitmap==null)
    		photoBitmap=new PhotoBitmap(fullPath);
        return photoBitmap;
    }
	public boolean isSend() {
		return id!=0;
	}
	//to prevent double sending
	boolean sendLock=false;
	public boolean tryLock(){
		boolean oldValue=sendLock;
		sendLock=true;
		return oldValue;
	};
	public boolean isLocked(){
		return sendLock;
	};
}
