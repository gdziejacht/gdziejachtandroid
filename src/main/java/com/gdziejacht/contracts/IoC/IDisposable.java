package com.gdziejacht.contracts.IoC;



@Internal
public interface IDisposable {
	void deactivate() throws Throwable;
}
