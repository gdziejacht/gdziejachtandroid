package com.gdziejacht.contracts;

import com.gdziejacht.DataModels.LocalCheckpoint;
import com.gdziejacht.DataModels.LocalPhoto;
import com.gdziejacht.DataModels.LocalTrip;
import com.gdziejacht.DataModels.RegisterData;

public interface IWebServiceConnector {

	void login(String login, String password);

	void register(RegisterData data);

	void sendPhoto(LocalPhoto photo);

	void sendCheckpoint(LocalCheckpoint checkpoint);

	void BeginTrip(LocalTrip localTrip);

}
