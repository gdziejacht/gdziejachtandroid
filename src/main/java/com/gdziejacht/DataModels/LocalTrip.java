package com.gdziejacht.DataModels;


import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.gdziejacht.PseudoContainer;
import com.gdziejacht.commons.AbstractProperty;
import com.gdziejacht.contracts.IDatabaseHelper;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class LocalTrip extends AbstractProperty {
	public static final String FK_LOCALUSER = "FKLocalUser";
	static IDatabaseHelper databaseHelper = PseudoContainer.Instance.getSingleton(IDatabaseHelper.class);
	@DatabaseField(generatedId = true)
	int genId;
	@DatabaseField(foreign=true, columnName=FK_LOCALUSER)
	LocalUser user;
	@DatabaseField
	private long startTime;// stored as UTC
	@DatabaseField
	private String info;
	@DatabaseField
	private int id;//value 0 means - not send to server;
	@ForeignCollectionField(eager = true)
	private ForeignCollection<LocalCheckpoint> checkpoints;
	@DatabaseField
	private boolean finished = false;

	public DateTime getStartTime() {
		return new DateTime(startTime, DateTimeZone.UTC);
	}

	public void setStartTime(DateTime startTime) {
		this.startTime = startTime.getMillis();
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
		databaseHelper.updateTrip(this);
	}


	public LocalTrip() {

	}

	public LocalCheckpoint getFirstCheckpoint() {
		LocalCheckpoint[] array = checkpoints.toArray(new LocalCheckpoint[0]);
		if(array.length>0)
			return array[0];
		return null;
	}

	public LocalCheckpoint getLastCheckpoint() {
		LocalCheckpoint[] array = checkpoints.toArray(new LocalCheckpoint[0]);
		if(array.length>0)
			return array[array.length-1];
		return null;
	}

	public LocalCheckpoint[] getCheckpoints() {
		return checkpoints.toArray(new LocalCheckpoint[0]);
	}

	public void addCheckpoint(LocalCheckpoint checkpoint) {
		checkpoints.add(checkpoint);
	}

	public void finish() {
		this.finished = true;
	}
	public LocalUser getUser() {
		return user;
	}

	public void setUser(LocalUser user) {
		this.user = user;
	}

	public boolean isFinished() {
		return finished;
	}
	public boolean isSend() {
		if(!finished||id==0)
			return false;
		for(LocalCheckpoint i:checkpoints){//its a eager collection, so its probably not so bad as it looked to be.
			if(!i.isSend())
				return false; 
		}
		return true;
	}
	public void deleteAllCheckpoints(){
		if(checkpoints!=null)
			checkpoints.clear();
	}
	public int getGenId() {
		return genId;
	}
	@Override
	public boolean equals(Object obj){
		if(obj instanceof LocalTrip)
			return ((LocalTrip) obj).genId==this.genId;
		return false;
	}
}
