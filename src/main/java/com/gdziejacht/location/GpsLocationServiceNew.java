package com.gdziejacht.location;

import com.gdziejacht.PseudoContainer;
import com.gdziejacht.DataModels.LocalCheckpoint;
import com.gdziejacht.contracts.IGpsLocationService;
import com.gdziejacht.contracts.IState;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;

public class GpsLocationServiceNew extends Service implements
		IGpsLocationService {
	IState state = PseudoContainer.Instance.getSingleton(IState.class);
	private float LISTENER_DISTANCE = 1f;
	private long LISTERER_TIME = 10000;

	private LocationManager lm;

	@Override
	public void onLocationChanged(Location location) {
		LocalCheckpoint checkpoint = new LocalCheckpoint();
		checkpoint.setLat(location.getLatitude());
		checkpoint.setLng(location.getLongitude());
		checkpoint.setAlt(location.getAltitude());
		checkpoint.setSpeed(location.getSpeed());
		checkpoint.setTime(location.getTime());
		state.setActualCheckpoint(checkpoint);
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, LISTERER_TIME,
				LISTENER_DISTANCE, this);
		if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER))// if we have a
																// fix with
																// position
		{
			Location actualPosition = lm
					.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			if (actualPosition != null)
				onLocationChanged(actualPosition);
			state.setGpsEnabled(true);
		}
		else
			state.setGpsEnabled(false);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		state.setGpsEnabled(true);

	}

	@Override
	public void onProviderDisabled(String provider) {
		state.setGpsEnabled(false);

	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

}
