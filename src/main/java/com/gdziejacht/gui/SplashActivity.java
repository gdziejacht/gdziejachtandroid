package com.gdziejacht.gui;

import com.gdziejacht.*;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.os.Handler;


public class SplashActivity extends SuperActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new Handler().postDelayed(new Thread() {
            @Override
            public void run() {
                SplashActivity.this.switchActivity(WelcomeActivity.class);
                SplashActivity.this.finish();
            }
        }, Constants.THREAD_DELAY);
    }
}
