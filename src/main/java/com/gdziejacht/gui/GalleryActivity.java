package com.gdziejacht.gui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import com.gdziejacht.DataModels.LocalPhoto;
import com.gdziejacht.PseudoContainer;
import com.gdziejacht.R;
import com.gdziejacht.adapters.LocalPhotoArrayAdapter;
import com.gdziejacht.contracts.IDatabaseHelper;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Piotr on 2014-11-11.
 */
public class GalleryActivity extends SuperActivity implements
		AdapterView.OnItemClickListener {
    private List<LocalPhoto> localPhotos;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gallery);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

	}
	protected void onResume(){
		super.onResume();
		createPhotosForGallery();
	}
	private void createPhotosForGallery() {
		// List<File> photosFiles = getAllPhotoFiles();
        loadAllPhotos();
		ListView listView = (ListView) findViewById(R.id.gallery_listview);
		TextView textView = (TextView) findViewById(R.id.photos_count_textview);
		textView.setText(localPhotos.size() + " zdjęć w galerii"); // TODO: przeniesc do string values?
		LocalPhotoArrayAdapter adp = new LocalPhotoArrayAdapter(
				GalleryActivity.this, R.layout.list, localPhotos);
		listView.setAdapter(adp);
		listView.setOnItemClickListener(this);
		listView.setClickable(true);

	}
    protected void loadAllPhotos() {
        //stub
        //TODO: ogarnac doczytywanie zdjec?
        //
        //if(localPhotos==null)

        //TODO: ogarnac dostawanie helpera za pomoca autowire
    	if(localPhotos!=null)
    		localPhotos.clear();
        IDatabaseHelper helper= PseudoContainer.Instance.getSingleton(IDatabaseHelper.class);
        localPhotos = helper.getAllPhotosByTrip(state.getVisibleTrip());
        if(localPhotos==null)
            localPhotos = new ArrayList<>();
    }

	/**
	 * zwraca zdjecia nalezace do X z przedzialu:
	 * startDate-secondIntervalEpsilon< X >endDate+secondIntervalEpsilon
	 * 
	 * @param startDate
	 * @param endDate
	 * @param secondIntervalEpsilon
	 * @return
	 */
//	public static List<File> getPhotosFromDateRange(DateTime startDate,
//			DateTime endDate, int secondIntervalEpsilon) {
//		return Arrays.asList(CameraActivity.getDir().listFiles(
//				new DateRangeFileFilter(startDate, endDate,
//						secondIntervalEpsilon)));
//	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
		Intent intent = new Intent(this, PhotoActivity.class);
		// haack
		final ListView listView = (ListView) findViewById(R.id.gallery_listview);
		intent.putExtra(PhotoActivity.PHOTO_EXTRA_INTENT_NAME,
				((LocalPhoto) (listView.getItemAtPosition(i))).getFullPath());
		// TODO: zrobic ladniej
		// intent.putExtra(PhotoActivity.PHOTO_EXTRA_INTENT_NAME,((LocalPhoto)((ListView)adapterView).getItemAtPosition(i)).getFullPath());
		startActivity(intent);
	}

//	private static class DateRangeFileFilter implements FilenameFilter {
//		DateTime startDate;
//		DateTime endDate;
//		int secondIntervalEpsilon;
//
//		DateRangeFileFilter(DateTime startDate, DateTime endDate,
//				int secondIntervalEpsilon) {
//			// TODO: dodac sprawdzanie czy endDate>startDate
//			this.startDate = new DateTime(startDate);
//			this.endDate = new DateTime(endDate);
//			this.secondIntervalEpsilon = secondIntervalEpsilon;
//			this.startDate.minusSeconds(secondIntervalEpsilon);
//			this.endDate.plusSeconds(secondIntervalEpsilon);
//		}
//
//		@Override
//		public boolean accept(File dir, String filename) {
//			// String lowercaseName = filename.toLowerCase();
//			DateTime fileTime = DateTime.parse(filename,
//					CameraActivity.DATE_TIME_FORMATTER);
//			return fileTime.isAfter(startDate) && fileTime.isBefore(endDate);
//		}
//	}
}
