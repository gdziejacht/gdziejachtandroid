package com.gdziejacht;

import java.util.List;

import org.joda.time.DateTime;

import com.gdziejacht.DataModels.LocalCheckpoint;
import com.gdziejacht.DataModels.LocalTrip;
import com.gdziejacht.DataModels.LocalUser;
import com.gdziejacht.commons.AbstractProperty;
import com.gdziejacht.contracts.IDatabaseHelper;
import com.gdziejacht.contracts.IState;
import com.gdziejacht.contracts.IWebServiceConnector;
import com.gdziejacht.contracts.IoC.Autowired;
import com.gdziejacht.contracts.IoC.IActivate;

public class State extends AbstractProperty implements IState, IActivate {

	private IDatabaseHelper databaseHelper;
	private IWebServiceConnector connector;
	private boolean ok=false;
	@Autowired
	public void setDatabaseHelper(IDatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;

	}

	@Autowired
	public void setWebServiceConnector(IWebServiceConnector connector) {
		this.connector = connector;

	}

	@Override
	public void activate() {
		ok=true;
	}
	public boolean isOk(){
		return ok;
	}
	@Override
	public LocalTrip createTrackingTrip() {
		actualTrackingTrip = new LocalTrip();
		actualTrackingTrip.setStartTime(new DateTime());
		actualTrackingTrip.setUser(loggedUser);
		databaseHelper.createTrip(actualTrackingTrip);
		actualTrackingTrip = databaseHelper.getTrip(actualTrackingTrip);// ugly hack to acquire
														// checkpoint list DAO
		actualTrackingTrip.setUser(loggedUser);//set a one more time
		connector.BeginTrip(actualTrackingTrip);
		if (actualCheckpoint != null) {
			actualCheckpoint.setTrip(actualTrackingTrip);
			actualTrackingTrip.addCheckpoint(actualCheckpoint);
			//databaseHelper.createCheckpoint(actualCheckpoint);
			connector.sendCheckpoint(actualCheckpoint);
		}

		listeners.firePropertyChange(ACTION_BEGINTRIP, null, actualTrackingTrip);
		return actualTrackingTrip;
	}
	@Override
	public void deleteTrip(LocalTrip trip) {
		databaseHelper.deleteTrip(trip);
	}
	@Override
	public void finishTrackingTrip() {
		actualTrackingTrip.finish();
		databaseHelper.updateTrip(actualTrackingTrip);
		//tripList.add(actualTrackingTrip);
		actualTrackingTrip = null;
	}

	private LocalUser loggedUser;
	private LocalTrip actualTrackingTrip;
	private LocalTrip visibleTrip;
	private LocalCheckpoint actualCheckpoint;
	private boolean gpsEnabled = false;
	@Override
	public boolean isGpsEnabled() {
		return gpsEnabled;
	}

	@Override
	public void setGpsEnabled(boolean gpsState) {
		boolean previous = this.gpsEnabled;
		this.gpsEnabled = gpsState;
		listeners.firePropertyChange(ACTION_GPSSTATE_CHANGED, previous,
				gpsState);
	}

	@Override
	public LocalCheckpoint getActualCheckpoint() {
		return actualCheckpoint;
	}

	@Override
	public void setActualCheckpoint(LocalCheckpoint actualPosition) {
		LocalCheckpoint previous = actualCheckpoint;
		this.actualCheckpoint = actualPosition;
		if (actualTrackingTrip != null) {
			actualPosition.setTrip(actualTrackingTrip);
			// databaseHelper.createCheckpoint(actualPosition);
			actualTrackingTrip.addCheckpoint(actualCheckpoint);
			connector.sendCheckpoint(actualCheckpoint);
		}
		listeners.firePropertyChange(ACTION_POSITION_CHANGED, previous,
				actualPosition);
	}

	@Override
	public void setLoggedUser(LocalUser localuser) {
		if (localuser != null) {
			LocalUser queriedUser = databaseHelper.findUserById(localuser);
			if (queriedUser != null) {
				queriedUser.setName(localuser.getName());
				queriedUser.setEmail(localuser.getEmail());
				databaseHelper.updateUser(queriedUser);
				localuser=queriedUser;
			} else {
				databaseHelper.createUser(localuser);
			}
		}
		LocalUser previous = this.loggedUser;
		this.loggedUser = localuser;
		listeners.firePropertyChange(ACTION_LOGIN, previous, loggedUser);
	}

	@Override
	public LocalUser getLoggedUser() {
		return loggedUser;
	}

	@Override
	public LocalTrip getActualTrip() {
		return actualTrackingTrip;
	}

	@Override
	public List<LocalTrip> getAllTrips() {
		return databaseHelper.getAllTripsByUser(loggedUser);
	}
	@Override
	public LocalTrip getVisibleTrip() {
		if(visibleTrip==null)
			return actualTrackingTrip;
		else
			return visibleTrip;
	}
	@Override
	public void setVisibleTrip(LocalTrip visibleTrip) {
		//if(visibleTrip!=null)
		this.visibleTrip = visibleTrip;
	}

	@Override
	public void reSendVisibleTrip() {
		LocalCheckpoint[] checkpoints = visibleTrip.getCheckpoints();
		if(checkpoints.length==0)
			return; //abort sending empty trips
		if(this.visibleTrip.getId()==0){
			visibleTrip.setUser(loggedUser);
			databaseHelper.updateTrip(visibleTrip);
			connector.BeginTrip(visibleTrip);
		}
		for(LocalCheckpoint i:checkpoints)
		{
			i.setTrip(visibleTrip);//its really necessary
			connector.sendCheckpoint(i);
		}
	}

	@Override
	public void setLoggedUser(int userId) {
		//relogin functionality
		LocalUser localuser=new LocalUser();
		localuser.setId(userId);
		LocalUser queriedUser = databaseHelper.findUserById(localuser);
		if(queriedUser!=null){
			this.loggedUser = queriedUser;
			List<LocalTrip> trips = getAllTrips();
			if(trips!=null&&trips.size()>0)
			{
				LocalTrip lastTrip = trips.get(trips.size()-1);
				if(!lastTrip.isFinished()){
					this.actualTrackingTrip=lastTrip;//trip recover
				}
			}
		}
		
	}
	
}
