package com.gdziejacht.commons;

import android.os.AsyncTask;


public class ThreadHelper {
public static void launchInBackground(final Runnable runnable){
    new AsyncTask<Void, Void, Void>(){

		@Override
		protected Void doInBackground(Void... arg0) {
			runnable.run();
			return null;
		}
    }.execute();
	
}
}
