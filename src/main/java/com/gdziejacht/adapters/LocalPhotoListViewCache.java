package com.gdziejacht.adapters;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.gdziejacht.R;

public class LocalPhotoListViewCache { //na potrzeby oszczedzania CPU i MEM

    private View baseView;
    private TextView text1View;
    private Button button;
    private ImageView imageView;

    public LocalPhotoListViewCache ( View baseView ) {
        this.baseView = baseView;
    }

    public View getViewBase ( ) {
        return baseView;
    }

    public TextView getText1View (int resource) {
        if ( text1View == null ) {
            text1View = ( TextView ) baseView.findViewById(R.id.text1_photoarrayadapter);
        }
        return text1View;
    }

    public Button getButton(int resource) {
        if(button==null) {
            button=(Button) baseView.findViewById(R.id.btnSendPhotos) ;
        }
        return button;
    }

    public ImageView getImageView (int resource) {
        if ( imageView == null ) {
            imageView = ( ImageView ) baseView.findViewById(R.id.image_photoarrayadapter);
        }
        return imageView;
    }
}
