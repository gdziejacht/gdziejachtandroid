package com.gdziejacht.DataModels;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.*;

//import java.io.ByteArrayOutputStream;

public class PhotoBitmap {
	private String path;
	private Bitmap photo;

	public Bitmap getFullSizePhoto() {
		if (photo == null)// lazy loading
			try {
				this.photo = BitmapFactory.decodeFile(path);
			} catch (Exception e) {
				return null;
			}
		return photo;
	}
    public Bitmap getSampledSizePhoto(int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }
	public byte[] getFileData() {
		File file = new File(path);
	    int size = (int) file.length();
	    byte[] bytes = new byte[size];
	    try {
	        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
	        buf.read(bytes, 0, bytes.length);
	        buf.close();
	    } catch (FileNotFoundException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	        return null;
	    } catch (IOException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	        return null;
	    }
	    return bytes;
//		ByteArrayOutputStream stream = new ByteArrayOutputStream();
//		if (!getFullSizePhoto().compress(Bitmap.CompressFormat.JPEG, 70, stream))
//			return null;
//		byte[] byteArray = null;
//		try {
//			byteArray = stream.toByteArray();
//			stream.close();
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return byteArray;
	}

	public PhotoBitmap(String path) {
		this.path = path;
	}
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
