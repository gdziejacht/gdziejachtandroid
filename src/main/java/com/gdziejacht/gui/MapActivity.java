package com.gdziejacht.gui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.gdziejacht.DataModels.LocalCheckpoint;
import com.gdziejacht.R;
import com.gdziejacht.contracts.IState;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.*;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

/**
 * Created by Piotr on 2014-11-11.
 */
public class MapActivity extends SuperActivity {
	private GoogleMap mMap;
	private Marker actualMarker;
	private Marker lastMarker;
	private Marker firstMarker;
	private Polyline tripLine;
	private boolean trackNewCheckpoints = true;
	private LatLngBounds.Builder mapBoundsBuilder ;//= new LatLngBounds.Builder();

	/*
	 * właściwie to dodaję to za każdym razem, gdy siadam do kodu, a bez
	 * Contextu (poza activity) alert nie chce działać, więc tworzenie
	 * jakiegoś GuiHelpera mija się z celem. Proszę zostawić.
	 */
	public void Alert(String msg) {
		AlertDialog dialog = new AlertDialog.Builder(this).create();
		dialog.setMessage(msg);
		dialog.show();
	}

	private PropertyChangeListener checkpointTracker = new PropertyChangeListener() {
		@Override
		public void propertyChange(PropertyChangeEvent event) {
			if (event.getNewValue() != null) {
				LocalCheckpoint checkpoint = (LocalCheckpoint) event
						.getNewValue();
				if (actualMarker == null)
					actualMarker = mMap.addMarker(new MarkerOptions()
							.position(
									new LatLng(checkpoint.getLat(), checkpoint
											.getLng())).title(
									getResources().getString(
											R.string.you_are_here)));
				else
					actualMarker.setPosition(new LatLng(checkpoint.getLat(),
							checkpoint.getLng()));

				if (trackNewCheckpoints)
					mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(
							checkpoint.getLat(), checkpoint.getLng())));

				if (state.getActualTrip() == null
						|| state.getActualTrip() != state.getVisibleTrip())
					return;

				if (lastMarker == null) {
					lastMarker = mMap
							.addMarker(new MarkerOptions().position(
									new LatLng(checkpoint.getLat(), checkpoint
											.getLng()))
									.title(getResources().getString(
											R.string.trip_end)));
					firstMarker = mMap.addMarker(new MarkerOptions()
							.position(
									new LatLng(checkpoint.getLat(), checkpoint
											.getLng())).title(
									getResources().getString(
											R.string.trip_start)));
				} else
					lastMarker.setPosition(new LatLng(checkpoint.getLat(),
							checkpoint.getLng()));

				if (tripLine == null) {
					LocalCheckpoint[] checkpoints = state.getActualTrip()
							.getCheckpoints();
					PolylineOptions line = new PolylineOptions();
					for (int i = 0; i < checkpoints.length; i++) {
						LatLng newPoint = new LatLng(checkpoints[i].getLat(),
								checkpoints[i].getLng());
						line.add(newPoint);
						mapBoundsBuilder.include(newPoint);
					}
					line.width(5);
					tripLine = mMap.addPolyline(line);
				} else {
					List<LatLng> points = tripLine.getPoints();
					points.add(new LatLng(checkpoint.getLat(), checkpoint
							.getLng()));
					mapBoundsBuilder.include(new LatLng(checkpoint.getLat(),
							checkpoint.getLng()));
					tripLine.setPoints(points);
				}

				if (!trackNewCheckpoints)
					mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(
							mapBoundsBuilder.build(), 10));
			}
		}
	};

	private OnClickListener stopListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			state.finishTrackingTrip();
			toggleButton(false);
			// TODO //clean up screen, go back to route info?
		}
	};

	private OnClickListener startListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			state.createTrackingTrip();
			drawActualTrip();

			toggleButton(true);
		}
	};
	@Override
	public void onCreate(Bundle savedInstanceState) {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);
		setUpMapIfNeeded();
		Button pinButton = (Button) findViewById(R.id.pin_button);
		pinButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				if (state.getActualTrip() == null
						|| state.getActualCheckpoint() == null) {
					AlertDialog.Builder builder1 = new AlertDialog.Builder(
							MapActivity.this);
					// TODO: przeniesc do string values?
					builder1.setMessage("Nie można robić zdjęć bez włączonej trasy.");
					builder1.setCancelable(true);
					builder1.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							});
					AlertDialog alert11 = builder1.create();
					alert11.show();
					return;
				}
				MapActivity.this.switchActivity(CameraActivity.class);
			}

		});
		// if(state.getActualTrip()==state.getVisibleTrip())
		state.addPropertyChangeListener(IState.ACTION_POSITION_CHANGED,
				checkpointTracker);



		//drawActualTrip();

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		final Button centerButton = (Button) findViewById(R.id.btnCenter);
		// switchIcon
		int resource = trackNewCheckpoints ? R.drawable.crossbar_active
				: R.drawable.crossbar_nonactive;
		centerButton.setBackgroundResource(resource);
		centerButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				trackNewCheckpoints = !trackNewCheckpoints;
				// switchIcon
				int resource = trackNewCheckpoints ? R.drawable.crossbar_active
						: R.drawable.crossbar_nonactive;
				centerButton.setBackgroundResource(resource);
				// generate centering
				if (actualMarker != null) {
					if (!trackNewCheckpoints)
						mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(
								mapBoundsBuilder.build(), 10));
					else
						mMap.moveCamera(CameraUpdateFactory
								.newLatLng(new LatLng(state
										.getActualCheckpoint().getLat(), state
										.getActualCheckpoint().getLng())));
				}

			}

		});

	}
	private void setButton(){
		if (state.getVisibleTrip() != null) {
			if (!state.getVisibleTrip().isFinished()) {
				findViewById(R.id.btnStart).setEnabled(true);
				trackNewCheckpoints = true;
			} else {
				findViewById(R.id.btnStart).setEnabled(false);
				trackNewCheckpoints = false;
			}
			toggleButton(true);
		} else
			toggleButton(false);
	}
	private void toggleButton(boolean start) {
		Button startButton = (Button) findViewById(R.id.btnStart);

		if (start) {
			startButton.setText(getResources().getString(R.string.stop));
			startButton.setOnClickListener(null);
			startButton.setOnClickListener(stopListener);
		} else {
			startButton.setText(getResources().getString(R.string.start));
			startButton.setOnClickListener(null);
			startButton.setOnClickListener(startListener);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		setUpMapIfNeeded();
		setButton();
		drawActualTrip();
	}

	private void setUpMapIfNeeded() {
		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			mMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();
			// Check if we were successful in obtaining the map.
			// if (mMap != null) {
			// setUpMap();
			// }
		}
	}

	public void drawActualTrip() {
		mMap.clear();
		actualMarker = null;
		tripLine = null;
		mapBoundsBuilder= new LatLngBounds.Builder();
		if (state.getVisibleTrip() != null) {
			LocalCheckpoint[] checkpoints = state.getVisibleTrip()
					.getCheckpoints();
			if (checkpoints.length > 0) {
				PolylineOptions line = new PolylineOptions();
				for (int i = 0; i < checkpoints.length; i++) {
					LatLng newpoint = new LatLng(checkpoints[i].getLat(),
							checkpoints[i].getLng());
					line.add(newpoint);
					mapBoundsBuilder.include(newpoint);
				}
				line.width(5);
				mMap.addPolyline(line);
				firstMarker = mMap.addMarker(new MarkerOptions().position(
						new LatLng(state.getVisibleTrip().getFirstCheckpoint()
								.getLat(), state.getVisibleTrip()
								.getFirstCheckpoint().getLng())).title(
						getResources().getString(R.string.trip_start)));
				lastMarker = mMap.addMarker(new MarkerOptions().position(
						new LatLng(state.getVisibleTrip().getLastCheckpoint()
								.getLat(), state.getVisibleTrip()
								.getLastCheckpoint().getLng())).title(
						getResources().getString(R.string.trip_end)));

				// http://stackoverflow.com/questions/13692579/movecamera-with-cameraupdatefactory-newlatlngbounds-crashes
				mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
					@Override
					public void onMapLoaded() {
						mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(
								mapBoundsBuilder.build(), 10));
					}
				});
			}
		} else if (state.getActualCheckpoint() != null)
			// ugly, prevents to creating a empty mapBounds
			mapBoundsBuilder.include(new LatLng(state.getActualCheckpoint()
					.getLat(), state.getActualCheckpoint().getLng()));
		checkpointTracker.propertyChange(new PropertyChangeEvent(this,
				IState.ACTION_POSITION_CHANGED, null, state
						.getActualCheckpoint()));

	}
}
