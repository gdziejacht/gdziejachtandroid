#!/usr/bin/perl

use Geo::Gpx;
use warnings;
use utf8;
use strict;

open(my $INPUT, '<', $ARGV[0]) or die "Error in opening gpx file: $!";
binmode($INPUT, ":utf8");
open(OUTPUT, '>', 'sendGpx.sh');
binmode(OUTPUT, ":utf8");
print OUTPUT '{ ';
my $gpx = Geo::Gpx->new(input => $INPUT) or die "Error in opening gpx file: $!";
foreach my $track ( @{$gpx->tracks()} ) {
    foreach my $seg( @{$track->{'segments'}} ) {
    foreach my $points( @{$seg->{'points'}} ) {
	print OUTPUT 'echo "geo fix '.$points->{'lon'}.' '.$points->{'lat'},'";',' sleep 1;';
}
}
}
print OUTPUT ' }|telnet localhost 5554';
