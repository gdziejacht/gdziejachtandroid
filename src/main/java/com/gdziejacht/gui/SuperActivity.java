package com.gdziejacht.gui;

import com.gdziejacht.PseudoContainer;
import com.gdziejacht.R;
import com.gdziejacht.contracts.IState;
import com.gdziejacht.contracts.IWebServiceConnector;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * @author globalbus all activities should extend this activity
 */
public class SuperActivity extends FragmentActivity {
	private static final String USERID = "UserID";
	IWebServiceConnector connector;
	IState state;

	public SuperActivity() {
//		connector = PseudoContainer.Instance
//				.getSingleton(IWebServiceConnector.class);
//		state = PseudoContainer.Instance.getSingleton(IState.class);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		realloc();
	}
	public void onRestoreInstanceState(Bundle savedInstanceState) {
	    // Always call the superclass so it can restore the view hierarchy
	    super.onRestoreInstanceState(savedInstanceState);
	    int userId = savedInstanceState.getInt(USERID);
	   if(state==null)
		   realloc();
	   if(state.getLoggedUser()==null){
		   state.setLoggedUser(userId);
	   }
	}
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
	    // Save the user's current game state
		if(state.getLoggedUser()!=null)
	    savedInstanceState.putInt(USERID, state.getLoggedUser().getId());
	    
	    // Always call the superclass so it can save the view hierarchy state
	    super.onSaveInstanceState(savedInstanceState);
	}
	protected void switchActivity(Class<? extends Activity> activity) {
		Intent login = new Intent(SuperActivity.this, activity);
		SuperActivity.this.startActivity(login);
		overridePendingTransition(R.layout.fadein, R.layout.fadeout);
	}

	// protected void switchActivity(Class<? extends Activity> activity, Bundle
	// b){
	// Intent login = new Intent(SuperActivity.this,
	// activity);
	// login.putExtras(b);
	// SuperActivity.this.startActivity(login);
	// overridePendingTransition(R.layout.fadein, R.layout.fadeout);
	// }

//	@Override
//	protected void onRestart() {
//		super.onRestart();
//		realloc();
//	}

	public void realloc() {
			PseudoContainer.Instance.registerApp(getApplicationContext());
		connector = PseudoContainer.Instance
				.getSingleton(IWebServiceConnector.class);
		state = PseudoContainer.Instance.getSingleton(IState.class);
	}
}
