package com.gdziejacht.contracts.IoC;

import com.gdziejacht.contracts.IProperty;

/**
 * @author globalbus
 * This interface was handled by Container to make java beans persistent
 */
public interface ISerializable extends IProperty {

}
