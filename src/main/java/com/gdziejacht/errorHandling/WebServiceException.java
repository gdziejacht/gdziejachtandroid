package com.gdziejacht.errorHandling;

import org.ksoap2.SoapFault;

@SuppressWarnings("serial")
public class WebServiceException extends Exception{
	

	public String type;
	public boolean internal=false;

	public WebServiceException(SoapFault fault) {
		super(fault.faultstring,fault);
		type = (String) fault.detail.getElement(0).getElement(0).getElement("http://schemas.datacontract.org/2004/07/System.ServiceModel", "Type").getChild(0);
		if(!type.equals("GdzieJachtWS.CustomException"))
			internal=true;
	}

}
